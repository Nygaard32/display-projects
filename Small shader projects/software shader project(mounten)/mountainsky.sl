surface mountainsky(color startcolor = (0.14,0.34,0.63); color hazecolor = (0.7,1.0,1.0), sunCol = (1.0,0.0,1.0))
{
	/*Creation of the sun*/
	float uu = (u - 0.5) * 8.0, vv = (v - 0.1) * 1.5;
	point P1 = point(uu,vv,0.0), P2 = point(0.0,0.0,0.0), Pp = P;
	float dist = distance(P1,P2);
	float distInver = 1.0 - dist;
	color Sun = color(1.0,0.0,1.0);
	Sun = clamp(distInver,0.0,1.0)*sunCol;
	Sun += smoothstep(0,0.5,distInver);
	
	
	/*Original code*/
	color zerocol=color (0,1,1);
	
	float basecol=1-clamp((ycomp(P)-200)/500,0,1);
	color outcol = mix(zerocol, startcolor, basecol);

	float hazecol=pow(1-clamp((ycomp(P)-200)/300,0,1),3);
	outcol += mix(zerocol, hazecolor, hazecol);
	
	Ci = Sun + clamp(outcol,0,1);
	Oi = Os;
} 

