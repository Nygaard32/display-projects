#ifndef snoise
#define snoise(p) (2 * (float noise(p)) - 1)
#endif
float fBm (point p; uniform float octaves, lacunarity, gain)
{
  uniform float amp = 1;
  varying point pp = p;
  varying float sum = 0;
  uniform float i;

  for (i = 0;  i < octaves;  i += 1) {
    sum +=  amp * snoise (pp);
    amp *=  gain;
    pp *=  lacunarity;
  }
  return sum;
}
float turbolence (point p; uniform float octaves, lacunarity, gain)
{
  uniform float amp = 1;
  varying point pp = p;
  varying float sum = 0;
  uniform float i;

  for (i = 0;  i < octaves;  i +=  1) {
    sum +=  abs(amp * snoise (pp));
    amp *=  gain;
    pp *=  lacunarity;
  }
  return sum;
}
float RidgedMultifractal(point p; uniform float octaves, lacunarity, gain, H, sharpness, threshold)
{
	float result, signal, weight, i, exponent;
	varying point PP = p;

	for( i = 0; i<octaves; i += 1 ) {
       	if ( i ==  0) {
          		signal = snoise( PP );
          		if ( signal < 0.0 ) signal =  - signal;
          		signal = gain - signal;
          		signal = pow( signal, sharpness );
          		result = signal;
          		weight = 1.0;
        	}else{
          		exponent = pow( lacunarity, ( - i*H) );
			PP = PP * lacunarity;
          		weight = signal * threshold;
          		weight = clamp(weight, 0, 1)    ;    		
          		signal = snoise( PP );
          		signal = abs(signal);
          		signal = gain - signal;
          		signal = pow( signal, sharpness );
          		signal *=  weight;
          		result +=  signal * exponent;
       		}
		}
		return(result);
}
void voronoi_f1f2_2d (float ss, tt; output float f1; output float f2;)
{
	float jitter = 1.0;
	float sthiscell = floor(ss)+0.5;
	float tthiscell = floor(tt)+0.5;
	float spos1, tpos1;
	float spos2, tpos2;
	f1 = f2 = 1000;
	uniform float i, j;
	for (i = -1;  i <= 1;  i += 1) {
	 float stestcell = sthiscell + i;
		for (j = -1;  j <= 1;  j += 1) {
			float ttestcell = tthiscell + j;
			float spos = stestcell + jitter * (cellnoise(stestcell, ttestcell) - 0.5);
			float tpos = ttestcell + jitter * (cellnoise(stestcell+23, ttestcell - 87) - 0.5);
			float soffset = spos - ss;
			float toffset = tpos - tt;
			float dist = soffset*soffset + toffset*toffset;
			if (dist < f1) { 
				f2 = f1;
				f1 = dist;
			} else if (dist < f2) {
				f2 = dist;
			}
		}
	}
	f1 = sqrt(f1);  f2 = sqrt(f2);
}
surface mountain ()
{	
	float magnitude = (RidgedMultifractal((P + 100) * 0.002, 7, 1.5, 0.9, 0.8, 5, 8) * 120.00001); 
	/*Variabels for different opperations*/
	float XOffzet = 15, OffzetSize = 1000.0;
	//MountainHeightZ hasn't that big of a visual effect if it isn't negativ
	float LandScapeHight = 1000.0, MountainHeightZ = 100.0; 
	float RavinPmulti = 0.009, RavinOctaves = 5,RavinLacunarity  = 2.15, RavinGain = 0.6; 
	float RavinWidth = 50;
	float FogAmountStart = -40, FogAmountEnd = -10;
	float CrackMulti = 0.02, CrackOctaves = 5, CrackLacunarity = 2,CrackGain = 0.5 , AmountOfCracks = 20;
	float FogDencit = 800; //recomened
	/*Diffuse parameters*/
	float Darkening = 0.5, Ligthening = 1.5;
	/*----END OF VARIABEL DECELRATION----*/
	/*Creation of gorge*/
	float RavinPositionX = distance(xcomp(P), XOffzet) / OffzetSize;
	float d3 = RavinPositionX;
	RavinPositionX = smoothstep(0.8, 1.0, 1 - RavinPositionX);
	float d2 = distance(zcomp(P), MountainHeightZ) / LandScapeHight;
	float Ravin = sin(smoothstep(0, 1, 1 - turbolence(P*RavinPmulti, RavinOctaves, RavinLacunarity, RavinGain)));
	float mountainAtt = (d3*d2);
	Ravin =  (Ravin*RavinPositionX);
	
	/*Applying gorge to the area*/
	float mount = ((magnitude*mountainAtt) - (Ravin * RavinWidth));
	/*Fog amount*/
	float AmountOfFog = smoothstep( FogAmountStart, FogAmountEnd, mount);
	P += (mount*normalize(N));
	N = calculatenormal(P);
	normal Nf = faceforward (normalize(N), I);
	/*Colors*/
	color LandCol = (0.2, 0.2, 0.2), snow = (1, 0.980392, 0.980392),
	DawnCol = (0.2, 0.1, 0), FogCol = (0.6,1,1), DistFog = (0.6,1,1);	
	/*For creation of cracks*/
	float ff=abs(fBm(P*CrackMulti, CrackOctaves, CrackLacunarity, CrackGain))* AmountOfCracks;
	float f1,f2;
	/*Size of cracks and consistency*/
	//voronoi_f1f2_2d(xcomp(P+ff)*0.03,zcomp(P+ff)*0.03,f1,f2);	
	/*Creation of cracks and giving a color*/
	float crackc=smoothstep(0.0,0.05,abs(f1-f2));
	snow=mix(snow,color(0.8,0.8,0.8));
	point pp=P;
	pp += (normalize(N))*5.0;
	N = calculatenormal(pp);
	
	float LightYComp = (1 - ycomp(N)) * clamp(d3, 0, 1);
	color Diffuse = Darkening * (Ligthening * diffuse(Nf));  
	/*Mixar*/ 
	/*snow multipliec with LightYcomp so you can see the cracks. LandCol is a base color that is darker and creates the shadows*/
	color SnowMix = mix(LandCol, snow * (1 - LightYComp), smoothstep(25, 100, magnitude));
	/*Smoothstep over fog so fog in the gorge won't cover the howl landscape*/
	color col = mix(FogCol, SnowMix*Diffuse, smoothstep(0, 1, AmountOfFog));	
	/*Fog from moutains in the back*/
	float dimma=zcomp(P)/FogDencit;
	col=mix(col,DistFog,dimma);
	
	Ci = col;
	Oi=  Os;  
}

