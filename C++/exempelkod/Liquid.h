#pragma once
#include "SF_src\MyVector.h"
#include "SF_src\DemoHandler.h"
#define LiquidBORDER 5.0

class Liquid
{
public:
	Liquid(float length, float width, float height);
	~Liquid();

	float getDensity();
	float getArea();
	float getVolume();
	float getMass();
	float getHeight();
	float getWidth();
	float getLength();

	//void update();
	void changeLength(float value);
	void changeWidth(float value);
	void changeHeight(float value);
	void changeMass(float value);
	void setDensity(float set);
	void draw(DemoHandler* draw);
private:
	float _Length;
	float _Density;
	float _Area = NULL;
	float _Volume; // Cubic meters
	float _Mass; // Kg
	float _Height;
	float _Liters; // L = KG
	float _Width;

	bool onTime = true;

	void calcDensity();	// Mass / Volume
	void calcArea();	// l * w
	void calcVolume();	// Area * h
};

