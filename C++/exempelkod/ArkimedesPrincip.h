#pragma once
#include "SF_src\Demo.h"
#include <cmath>
#include <iostream>
#include "Liquid.h"
#include "Ball.h"

#define MULTIPLIER 2
#define MAKENEGATIVE *-1
#define RADCHANGER 0.01
#define MASSCHANGER 0.01
#define LiquidVALUECHANGER 0.01

class ArkimedesPrincip :
	public Demo
{
public:
	ArkimedesPrincip();
	~ArkimedesPrincip();

	void reset();
	void update(DemoHandler* draw);

	const virtual std::string getInfo();
	const virtual std::string getName();
private:
	Ball* _Ball = NULL;
	Liquid* _Liquid = NULL;

	float getRightVol(); //Used to calc the area of the ball that is in the Liquid
	void calcBuoyancy(); //Force = V(A instead) * density(Liquid) * GRAVITY
	void onClick(DemoHandler* draw);
	void inLiquidCheck();

	float _Force = 0;
	float _newVol = 0; //Used to calc the volume of the specific part under the Liquidmark
	float _pv = 0; // Density * volume = mass

	float _ballCreationMass = 0.01f;
	float _ballCreationRad = 0.5f;

	bool once = true;

	///
	//@Todo: maybe add more private variables
	///
};

