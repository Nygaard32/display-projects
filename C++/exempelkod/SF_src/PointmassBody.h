#pragma once
#include "../SF_src/Demo.h"
#include "../SF_src/DemoHandler.h"
#include "../SF_src/MyVector.h"
#include "../SF_src/MyMatrix.h"
#include "MyQuaternion.h"
#include <cmath>
#include <string>
#include <sstream>

//const static float BIGFLOAT=99999999;
//const static float SMALLTHRESHOLD=0.001;


//Används för ritandet. Lagrar tre index till vertex i kroppen som utgör en triangel
class Triangle
{
public:
    int mP0, mP1, mP2;//index till vertex
    Triangle(int p0, int p1, int p2){mP0 = p0; mP1 = p1; mP2 = p2;};
};

//hjälpklass som lagrar punktmassor som vi bygger vår fasta kropp av
//vi skulle kunnat använda Ball-klassen men den är lite overkill
class PointMass
{
public:
    float mMass;
    MyVector mPos;
    PointMass(const float m, const MyVector p)
    {
        mMass = m;
        mPos = p;
    }
};
class PointmassBody
{
public:
    MyVector mPos; //tyngdpunktens globala position
    MyVector mMomentum; //rörelsemängd (betecknas normalt p)
    MyVector mForce; //kraften som verkar på kroppen
    
    float mMass; //kroppens massa
    float mInvMass; //den inverterade massan
    
    
    MyQuaternion mOrient; //orientering
    MyVector mL;//rörelsemängdsmoment
    
    MyVector mTorque; //summan av momenten
    
    MyMatrix mIinvLocal; //inversen av tröghetsmomentet
    
    std::vector<PointMass*> mPointmasses;//punktmassorna
    std::vector<Triangle> mFaces;//Används vid ritning
    
    PointmassBody(std::vector<MyVector> pts, std::vector<Triangle> faces, float mass)
    {
        mFaces = faces;
        
        mMass = mass;
        mInvMass = 1.0/mass;
        float pointMass = mass / pts.size();
        
        //räkna ut tyngdpunkten och ta den som kroppens position
        mPos = MyVector();
        for(int i = 0; i < pts.size(); i++)
            mPos += (pts[i] * pointMass);//skala position med punktmassa
        mPos *= mInvMass;//dela med totala massan
        
        clearForce();
        
        mOrient = MyQuaternion();
        mL = MyVector();
        
        mPointmasses.clear();
        for(int i = 0; i < pts.size(); i++)
            mPointmasses.push_back(new PointMass(pointMass, pts[i] - mPos));
        
        compute_Iinv();
    }
    
    virtual ~PointmassBody()
    {
        for(int i = 0; i < mPointmasses.size(); i++)
            delete mPointmasses[i];
    }
    //beräknar tröghetsmomentet
    void compute_Iinv()
    {
        MyMatrix I;
        double pointMass = mMass / mPointmasses.size();//vi delar massan lika på alla punkter
        for(int i = 0; i < mPointmasses.size(); i++)
        {
            MyVector r = mPointmasses[i]->mPos;
            I.m11 +=  pointMass * (r.mY * r.mY + r.mZ * r.mZ);
            I.m22 +=  pointMass * (r.mX * r.mX + r.mZ * r.mZ);
            I.m33 +=  pointMass * (r.mX * r.mX + r.mY * r.mY);
            I.m12 += -pointMass * r.mX * r.mY;
            I.m13 += -pointMass * r.mX * r.mZ;
            I.m23 += -pointMass * r.mY * r.mZ;
        }
        I.m21 = I.m12;//vi sparar några beräkningar genom att utnyttja symetriegenskaper
        I.m31 = I.m13;
        I.m32 = I.m23;
        mIinvLocal = I;
        
        mIinvLocal.invert();
    }
    
    //Returnerar tröghetsmomentet i globalt koordinatsystem
    MyMatrix getIinv_global()
    {
        MyMatrix R = mOrient.toMatrix();
        MyMatrix RT = R;
        RT.transpose();
        return R * mIinvLocal * RT;//tröghetsmomentet roterat utifrån orienteringen
    }
    
    //en lokal position i globala koordinater
    MyVector localToGlobal(MyVector v)
    {
        return mPos + mOrient.rotate(v);
    }
    
    //En global position i lokala koordinater
    MyVector globalToLocal(MyVector v)
    {
        return mOrient.rotateBack(v - mPos);
    }
    
    //Rensar aggregerade krafter och moment
    void clearForce()
    {
        mForce = MyVector();
        mTorque = MyVector();
    }
    
    //För krafter som verkar i tyngdpunkten
    void addForce(MyVector f)
    {
        mForce += f;
    }
    
    //för krafter som verkar i en godtycklig punkt
    void addForce(MyVector f, MyVector globalPoint)
    {
        mTorque  += (globalPoint - mPos) % f;
        mForce += f;//vi behöver även lagra kraften - den kommer att påverka både linjär och rotationsrörelse
    }
    
    //ger hastighet för ett angivet tillstånd
    MyVector getVelocity()
    {
        return mMomentum * mInvMass;
    }
    
    //Ger hastigheten i en specifik (global) punkt
    MyVector getVelocity(MyVector pt)
    {
        return getVelocity() + getAngularVelocity() % (pt - mPos);
    }
    
    //Ger vinkelhastighet för ett angivet tillstånd
    MyVector getAngularVelocity()
    {
        //vi ska ta Iinv gånger L eftersom L = Iw. Alltså är w = Iinv * L
        return getIinv_global() * mL;
    }    
    
    //Uppdaterar orientering och rörelsemängdsmoment genom att applicera ett eulersteg
    void update(float dt)
    {
        mPos += getVelocity() * dt;
        mMomentum += mForce * dt;
        mMomentum *= 0.995; //dämpning av rörelsemängd
        
        MyQuaternion w = MyQuaternion( getAngularVelocity() );
        mOrient += w * mOrient * 0.5 * dt;//euler på orientering
        
        mOrient.normalize();
        mL += mTorque * dt;//euler på rörelsemängsmoment
        mL *= 0.995;//Dämpning av rotation
    }
    
    //Ger den närmsta hörnet (punkten) i kroppen till den angivna (globala) positionen
    //Denna används för att hitta punkter att dra i
    MyVector getClosestLocalPoint(MyVector fromGlobal)
    {
        MyVector fromLocal = globalToLocal(fromGlobal);
        MyVector closest = mPointmasses[0]->mPos;
        float dist = (fromLocal - closest).length();
        for(int i = 1; i < mPointmasses.size(); i++)
        {
            MyVector toTest = mPointmasses[i]->mPos;
            float tmpDist = (fromLocal - toTest).length();
            if(tmpDist < dist)
            {
                dist = tmpDist;
                closest = toTest;
            }
        }
        return closest;
    }
    
    virtual void draw(DemoHandler* draw)
    {
        //rita punktmassorna
        for(int i = 0;i < mPointmasses.size();i++)
            draw->drawPoint(localToGlobal( mPointmasses[i]->mPos).toPoint(), CYAN,
                            mPointmasses[i]->mMass/30);//rita storleken i förhållande till massa
        
        //vi ritar tyngdpunkten
        draw->drawPoint(mPos.toPoint(), GREEN, 0.2);
        
        for(int i = 0; i < mFaces.size(); i++)
        {
            std::vector<Point> front;
            front.push_back(localToGlobal(mPointmasses[mFaces[i].mP0]->mPos).toPoint());
            front.push_back(localToGlobal(mPointmasses[mFaces[i].mP1]->mPos).toPoint());
            front.push_back(localToGlobal(mPointmasses[mFaces[i].mP2]->mPos).toPoint());
            draw->drawPolygon(front,  PINK);
            
            std::vector<Point> back;
            back.push_back(localToGlobal(mPointmasses[mFaces[i].mP0]->mPos).toPoint());
            back.push_back(localToGlobal(mPointmasses[mFaces[i].mP2]->mPos).toPoint());
            back.push_back(localToGlobal(mPointmasses[mFaces[i].mP1]->mPos).toPoint());
            draw->drawPolygon(back,  YELLOW);
        }
        
    }
};
/*
 (c)2014 Henrik Engström, henrik.engstrom@his.se
 This code may only be used by students in the Game Physics course at the University of Skövde
 Contact me if you want to use it for other purposes.
 */