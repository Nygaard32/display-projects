#pragma once
#include "../SF_src/Demo.h"
#include "../SF_src/DemoHandler.h"
#include "../SF_src/MyVector.h"
#include "../SF_src/MyMatrix.h"
#include "MyQuaternion.h"
#include <cmath>
#include <string>
#include <sstream>
#include "PointmassBody.h"
class Plane
{
public:
    MyVector mPointinplane; // en (godtycklig bortsett från ritandet) punkt i planet
    MyVector mNormal; // planets normal
    
    Plane(const MyVector Point,const MyVector Normal)
    {
        mPointinplane=Point;
        mNormal=Normal;
        mNormal.normalize();//better safe than sorry
    }
    
    
    virtual void handleCollision(PointmassBody* body)
    {
        for(int i = 0;i<body->mPointmasses.size();i++)
        {
            MyVector globalP = body->localToGlobal(body->mPointmasses[i]->mPos);
            MyVector p_q = globalP - mPointinplane;
            float project = p_q * mNormal;
            
            if(project < 0)//vi är i backen
            {
                double vrel = body->getVelocity(globalP) * mNormal;
                if(vrel<SMALLTHRESHOLD)
                {
                    MyVector ra = globalP - body->mPos;
                    float epsilon = 0.5;//elasticitetskoefficient
                    double j = ( -(1 + epsilon) * vrel) /
                    ( body->mInvMass + mNormal * ( ( body->getIinv_global() * ( ra % mNormal) ) % ra));
                    body->mMomentum += (mNormal * j);//rörelsemängden
                    body->mL += (ra % (mNormal * j));//rörelsemängdsmomentet
                }
            }
        }
    }
    
    void draw(DemoHandler* draw)
    {
        draw->drawPlane( mPointinplane.toPoint(), mNormal.toPoint(), GREEN, 15 );
        draw->drawLine(mPointinplane.toPoint(), (mPointinplane+mNormal).toPoint(),WHITE);//Normalen
        
        
        // För att vi skall se något i x-y projektion
        MyVector lineDir=MyVector(0,0,1) % mNormal; //kryssprodukten av vektorn som pekar ut ur skärmen och planets normal
        lineDir.normalize();
        
        draw->drawLine((mPointinplane-lineDir*20).toPoint(), (mPointinplane+lineDir*20).toPoint(), GREEN);
        
    }
private:
	float SMALLTHRESHOLD = 0.001;
};

/*
 (c)2014 Henrik Engström, henrik.engstrom@his.se
 This code may only be used by students in the Game Physics course at the University of Skövde
 Contact me if you want to use it for other purposes.
 */