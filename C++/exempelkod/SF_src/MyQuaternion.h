
#pragma once
#include <cmath>
#include "../SF_src/MyVector.h"
#include "../SF_src/MyMatrix.h"
// En implementation av kvaternioner - framför allt avsedda för att representera orientering
class MyQuaternion
{
public:
    float mS,mX,mY,mZ;
    
    //konstruktor som skapar en kvaternion som inte roterar en vektor
    MyQuaternion()
    {
        mS = 1;
        mX = 0;
        mY = 0;
        mZ = 0;
    }
    
    MyQuaternion(const float s,const float x,const float y,const float z)
    {
        mS = s;
        mX = x;
        mY = y;
        mZ = z;
    }
    MyQuaternion(const MyQuaternion& v2)
    {
        mS = v2.mS;
        mX = v2.mX;
        mY = v2.mY;
        mZ = v2.mZ;
    }
    MyQuaternion(const MyQuaternion* v2)
    {
        mS = v2->mS;
        mX = v2->mX;
        mY = v2->mY;
        mZ = v2->mZ;
    }
    //initierar kvaternionen till att representera en rotation runt angiven axel med angivet antal grader (obs ej rad)
    //Se 9.2.4 i Ians bok
    MyQuaternion(const int degrees,const MyVector rotDir)
    {
        MyVector dir = rotDir;
        dir.normalize();//för säkerhets skull
        float alpha = 3.1415926/180.0*degrees;//rotationsvinkel i radianer
        dir*= sin(alpha*0.5);//riktningen skalas med sinus för halva vinkeln
        mS = cos(alpha*0.5);
        mX = dir.mX;
        mY = dir.mY;
        mZ = dir.mZ;//skalärvärdet är cosinus för halva vinkeln
    }
    
    MyQuaternion(const MyVector v)
    {
        mS = 0;
        mX = v.mX;
        mY = v.mY;
        mZ = v.mZ;
    }
    
    ~MyQuaternion(void)
    {
    }
    
    //plus-operatorn gör elementvis addering
    MyQuaternion operator+(const MyQuaternion& v2)
    {
        return MyQuaternion(mS+v2.mS,mX+v2.mX,mY+v2.mY,mZ+v2.mZ);
    }
    
    MyQuaternion operator+= (const MyQuaternion& v2)
    {
        mS+= v2.mS;
        mX+= v2.mX;
        mY+= v2.mY;
        mZ+= v2.mZ;
        return *this;
    }
    
    //på samma sätt som plus
    MyQuaternion operator-(const MyQuaternion& v2)
    {
        return MyQuaternion(mS-v2.mS,mX-v2.mX,mY-v2.mY,mZ-v2.mZ);
    }
    
    MyQuaternion  operator-= (const MyQuaternion& v2)
    {
        mS-= v2.mS;
        mX-= v2.mX;
        mY-= v2.mY;
        mZ-= v2.mZ;
        return *this;
    }
    //multiplikationsoperatorn (liknar kryssprodukt för vektorer)
    MyQuaternion operator*(const MyQuaternion& q2)
    {
        float s = mS*q2.mS  -mX*q2.mX  -mY*q2.mY  -mZ*q2.mZ;
        float x = mS*q2.mX  +mX*q2.mS  +mY*q2.mZ  -mZ*q2.mY;
        float y = mS*q2.mY  -mX*q2.mZ  +mY*q2.mS  +mZ*q2.mX;
        float z = mS*q2.mZ  +mX*q2.mY  -mY*q2.mX  +mZ*q2.mS;
        return MyQuaternion(s,x,y,z);
    }
    void operator*= (const MyQuaternion& q2)
    {
        (*this) = (*this)*q2;
    }
    //ger en kvaternion skalad med angiven faktor
    MyQuaternion operator*(const float f)
    {
        return MyQuaternion(mS*f,mX*f,mY*f,mZ*f);
    }
    //skalar kvaternionen med angiven faktor
    MyQuaternion operator*= (const float f)
    {
        mS*= f;
        mX*= f;
        mY*= f;
        mZ*= f;
        return *this;
    }
    //Omvandlar en enhetskvaternion till en rotationsmatris
    MyMatrix toMatrix()
    {
        MyMatrix ret;
        ret.m11 = 1-2*mY*mY-2*mZ*mZ;
        ret.m21 = 2*mX*mY+2*mS*mZ;
        ret.m31 = 2*mX*mZ-2*mS*mY;
        ret.m12 = 2*mX*mY-2*mS*mZ;
        ret.m22 = 1-2*mX*mX-2*mZ*mZ;
        ret.m32 = 2*mY*mZ+2*mS*mX;
        ret.m13 = 2*mX*mZ+2*mS*mY;
        ret.m23 = 2*mY*mZ-2*mS*mX;
        ret.m33 = 1-2*mX*mX-2*mY*mY;
        return ret;
    }
    //ger kvaternionens längd
    float length()
    {
        return sqrt(mS*mS+mX*mX+mY*mY+mZ*mZ);
    }
    //ger en enhetskvaternion (förutsatt att vi inte har noll-kvaternionen)
    void normalize()
    {
        float l = length();
        if(l>0.000001){
            mS*= 1/l;
            mX*= 1/l;
            mY*= 1/l;
            mZ*= 1/l;
        }
    }
    //returnerar konjugatet av kvaternionen
    MyQuaternion conjugate() const
    {
        return MyQuaternion(mS,-mX,-mY,-mZ);
    }
    
    //Returnerar den angivna vektorn roterad med denna kvaternion. Förutsätter att anropade kvaternion har längd 1.
    MyVector rotate(const MyVector v)
    {
        MyQuaternion vq =  MyQuaternion(v);
        MyQuaternion res = (*this)*vq*(*this).conjugate();
        return  MyVector(res.mX,res.mY,res.mZ);
    }
    //Bekvämlighetsmetod. Roterar angiven vektor tillbaka samma vinkel och riktning som kvaternionen anger (inversen på rotate())
    MyVector rotateBack(const MyVector v)
    {
        MyQuaternion vq =  MyQuaternion(v);
        MyQuaternion res = (*this).conjugate()*vq*(*this);
        return  MyVector(res.mX,res.mY,res.mZ);
    }
};

/*
 (c)2012 Henrik Engström, henrik.engstrom@his.se
 This code may only be used by students in the Game Physics course at the University of Skövde 
 Contact me if you want to use it for other purposes.
 */