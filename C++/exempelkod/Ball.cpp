#include "Ball.h"


Ball::Ball(MyVector pos, MyVector velo, float mass, float rad)
{
	_Pos = pos;
	_Velocity = velo;
	_Mass = abs(mass);
	_Rad = abs(rad);
	if (mass = !0)
	{
		_InvMass = 1.0f / abs(mass);
	}
	else
	{
		_InvMass = 1.0f / BIGFLOAT;
	}

	if (_Rad != 0)
	{
		calcArea();
		calcVolume();
	}
}

Ball::~Ball()
{
}

void Ball::calcArea()
{
	_Area = 4.0f * M_PI * (_Rad * _Rad);
}

void Ball::calcVolume()
{
	_Volumn = (4.0f / 3.0f) * M_PI * (_Rad * _Rad * _Rad);
	calcDensity();
}

void Ball::calcDensity()
{
	_Density = _Mass / _Volumn;
}

void Ball::move(float deltaTime)
{
	_Pos += _Velocity * deltaTime;
	//std::cout << _FallingForce.mY << " -- "<< _Force.mY << "\n";
	_Velocity += (_FallingForce + _Force) * _InvMass * deltaTime;
	if (_InLiquid)
	{
		_Velocity *= DAMP;
	}
}

void Ball::addForce(MyVector force)
{
	_Force += force;
}

void Ball::fallingObjectForce(MyVector force)
{
	_FallingForce += force;
}

void Ball::update()
{
	fallingObjectForce(MyVector(0, _Mass * GRAVITY, 0));
	handleGround();
	move(STEP);
	_Force = MyVector();
	_FallingForce = MyVector();
}

void Ball::draw(DemoHandler* draw)
{
	draw->drawLine(_Pos.toPoint(), (_Pos + _Velocity).toPoint(), Color::GRAY, LINEWIDTH);
	draw->drawPoint(_Pos.toPoint(), RED, _Rad);
	draw->drawText(Point(0, 3.5, 0), "Ball values.");
	draw->drawText(Point(0, 3.25, 0), "Volume: " + std::to_string(_Volumn) + " m^3.");
	draw->drawText(Point(0, 3, 0), "Density: " + std::to_string(_Density) + " kg/m^3.");
	draw->drawText(Point(0, 2.75, 0), "Mass: " + std::to_string(_Mass) + " kg.");
	draw->drawText(Point(0, 2.5, 0), "Radius: " + std::to_string(_Rad) + " m.");

}

void Ball::setInLiquid(int set)
{
	if (set == 1)
	{
		_InLiquid = true;
	}
	else
	{
		_InLiquid = false;
	}
}

void Ball::changeMass(float value)
{
	_Mass += value;
	if (_Mass <= 0)
	{
		_Mass = 0.01f;
	}
	calcDensity();
}

void Ball::changeRad(float value)
{
	_Rad += value;

	if (_Rad <= 0)
	{
		_Rad = 0.1f;
	}
	calcArea();
	calcVolume();
}

void Ball::handleGround(){
	if (_Pos.mY < _Rad){
		_Pos.mY = _Rad;
		_Velocity = MyVector();
	}
}

void Ball::clearVelo()
{
	_Velocity = MyVector();
}

void Ball::setPos(MyVector pos)
{
	_Pos = pos;
}

MyVector Ball::getPos()
{
	return _Pos;
}

MyVector Ball::getVelocity()
{
	return _Velocity;
}

MyVector Ball::getForce()
{
	return _Force;
}

float Ball::getArea()
{
	return _Area;
}

float Ball::getDensity()
{
	return _Density;
}

float Ball::getMass()
{
	return _Mass;
}

float Ball::getRad()
{
	return _Rad;
}

float Ball::getVolume()
{
	return _Volumn;
}