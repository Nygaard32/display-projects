#include "Liquid.h"

Liquid::Liquid(float length, float width, float height)
{
	_Length = abs(length);
	_Height = abs(height);
	_Width = abs(width);

	if (_Length != 0 && _Height != 0 && _Width != 0)
	{
		calcArea();
		calcVolume();
	}
}

Liquid::~Liquid()
{
}

void Liquid::draw(DemoHandler* draw)
{
	draw->drawPlane(Point(LiquidBORDER, 0, 0), Point(0, 0, 1), Color::BLUE,5);
	draw->drawLine(Point(0, 0, 0), Point(LiquidBORDER * 2, 0, 0), Color::PINK);
	//draw->drawPlane(MyVector(0, 0, 5).toPoint(), MyVector(0, 0, 5).toPoint(), Color::BLUE);
	draw->drawText(Point(5, 3.5, 0), "Liquid values");
	draw->drawText(Point(5, 3.25, 0), "Volume: " + std::to_string(_Volume) + " m^3.");
	draw->drawText(Point(5, 3, 0), "Density: " + std::to_string(_Density) + " kg/m^3.");
	draw->drawText(Point(5, 2.75, 0), "Mass: " + std::to_string(_Mass) + " kg.");
	draw->drawText(Point(5, 2.5, 0), "Length: " + std::to_string(_Length) + " m.");
	draw->drawText(Point(5, 2.25, 0), "Width: " + std::to_string(_Width) + " m.");
	draw->drawText(Point(5, 2.0, 0), "Height: " + std::to_string(_Height) + " m.");
}

void Liquid::calcArea()
{
	_Area = _Length * _Width;
}

void Liquid::calcVolume()
{
	if (_Area != NULL)
	{
		_Volume = _Area * _Height;
		if (onTime)
		{
			_Liters = _Volume * pow(10, 3);
			_Mass = _Liters;
			onTime = false;
		}
		
		calcDensity();
	}
	
}

void Liquid::calcDensity()
{
	_Density = _Mass / _Volume;
}

float Liquid::getDensity()
{
	return _Density;
}

float Liquid::getArea()
{
	return _Area;
}

float Liquid::getVolume()
{
	return _Volume;
}

float Liquid::getMass()
{
	return _Mass;
}

float Liquid::getHeight()
{
	return _Height;
}

float Liquid::getLength()
{
	return _Length;
}

float Liquid::getWidth()
{
	return _Width;
}

void Liquid::changeHeight(float value)
{
	_Height += value;
	if (_Height <= 0)
	{
		_Height = 0.01f;
	}
	calcArea();
	calcVolume();
}

void Liquid::changeLength(float value)
{
	_Length += value;
	if (_Length <= 0)
	{
		_Length = 0.01f;
	}
	calcArea();
	calcVolume();
}

void Liquid::changeWidth(float value)
{
	_Width += value;
	if (_Width <= 0)
	{
		_Width = 0.01f;
	}
	calcArea();
	calcVolume();
}

void Liquid::changeMass(float value)
{
	_Mass += value;
	calcDensity();
}

void Liquid::setDensity(float set)
{
	_Density = set;
}