#pragma once
#define _USE_MATH_DEFINES
#include "SF_src\DemoHandler.h"
#include "SF_src\MyVector.h"
#include <iostream>
#include <math.h>

#define STEP 1.0/60.0
#define BIGFLOAT 99999.0
#define GRAVITY -9.82
#define DAMP 0.99
#define LINEWIDTH 0.05

class Ball
{
public:
	Ball(MyVector pos, MyVector velo, float mass = 1, float rad = 1	);
	~Ball();
	
	float getArea();
	float getMass();
	float getRad();
	float getDensity();
	float getVolume();
	
	MyVector getVelocity();
	MyVector getPos();	
	MyVector getForce();
	
	void addForce(MyVector force);
	void draw(DemoHandler* draw);
	void update();
	void setInLiquid(int set); // Activates a dampening effect on ball
	void changeMass(float value);
	void changeRad(float value);
	void setPos(MyVector pos);
	void clearVelo();
private:
	MyVector _Pos;
	MyVector _Velocity;
	MyVector _Force;	//Buoyancy force
	MyVector _FallingForce; //Falling ball force
	
	float _Mass;
	float _InvMass;
	float _Rad;
	float _Area;
	float _Density;
	float _Volumn; 

	bool _InLiquid = false;

	void calcDensity(); // Mass / Volume
	void calcVolume();	// (4.0f/3.0f) * PI * (rad * rad * rad)
	void calcArea();	// 4.0f * PI * (rad * rad)
	void move(float deltaTime);
	void fallingObjectForce(MyVector froce);
	void handleGround();
};

