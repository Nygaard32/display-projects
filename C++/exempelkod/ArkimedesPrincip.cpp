#include "ArkimedesPrincip.h"


ArkimedesPrincip::ArkimedesPrincip()
{
	reset();
}


ArkimedesPrincip::~ArkimedesPrincip()
{
	delete _Ball;
	delete _Liquid;
}

void ArkimedesPrincip::reset()
{
	_Ball = NULL;
	_Liquid = NULL;
	_pv = 0;
	_Force = 0;
	_newVol = 0;
	_ballCreationMass = 0.01f;
	_ballCreationRad = 0.5f;
	_Liquid = new Liquid(0.1, 0.1, 0.2);
	once = true;
}

void ArkimedesPrincip::update(DemoHandler* draw)
{
	if (draw->keyTyped('r') || draw->keyTyped('R'))
	{
		reset();
	}
	if (draw->isMouseDown())
	{
		onClick(draw);
	}

	//Keys when ball is created
	if (_Ball != NULL)
	{
		_Ball->update();
		_Ball->draw(draw);

		if (draw->keyTyped('d') || draw->keyTyped('D'))
		{
			_Ball->changeMass(-MASSCHANGER);
		}
		if (draw->keyTyped('f') || draw->keyTyped('F'))
		{
			_Ball->changeMass(MASSCHANGER);
		}
		if (draw->keyTyped('a') || draw->keyTyped('A'))
		{
			_Ball->changeRad(-RADCHANGER);
		}
		if (draw->keyTyped('s') || draw->keyTyped('S'))
		{
			_Ball->changeRad(RADCHANGER);
		}
	}
	else
	{
		if (draw->keyTyped('d') || draw->keyTyped('D'))
		{
			_ballCreationMass -= MASSCHANGER;
		}
		if (draw->keyTyped('f') || draw->keyTyped('F'))
		{
			_ballCreationMass += MASSCHANGER;
		}
		if (draw->keyTyped('a') || draw->keyTyped('A'))
		{
			_ballCreationRad -= RADCHANGER;
		}
		if (draw->keyTyped('s') || draw->keyTyped('S'))
		{
			_ballCreationRad += RADCHANGER;
		}
		draw->drawText(Point(0, 3.5, 0), "Ball start values.");
		draw->drawText(Point(0, 3.25, 0), "Mass: " + std::to_string(_ballCreationMass) + " kg.");
		draw->drawText(Point(0, 3, 0), "Radius: " + std::to_string(_ballCreationRad) + " m.");
	}

	//Keys when Liquid is created (is created at start)
	if (_Liquid != NULL)
	{
		_Liquid->draw(draw);
		if (draw->keyTyped('z') || draw->keyTyped('Z'))
		{
			_Liquid->changeHeight(-LiquidVALUECHANGER);
		}
		if (draw->keyTyped('x') || draw->keyTyped('X'))
		{
			_Liquid->changeHeight(LiquidVALUECHANGER);
		}
		if (draw->keyTyped('c') || draw->keyTyped('C'))
		{
			_Liquid->changeLength(-LiquidVALUECHANGER);
		}
		if (draw->keyTyped('v') || draw->keyTyped('V'))
		{
			_Liquid->changeLength(LiquidVALUECHANGER);
		}
		if (draw->keyTyped('b') || draw->keyTyped('B'))
		{
			_Liquid->changeWidth(-LiquidVALUECHANGER);
		}
		if (draw->keyTyped('n') || draw->keyTyped('N'))
		{
			_Liquid->changeWidth(LiquidVALUECHANGER);
		}
		if (draw->keyTyped('k') || draw->keyTyped('K'))
		{
			_Liquid->changeMass(-LiquidVALUECHANGER);
		}
		if (draw->keyTyped('l') || draw->keyTyped('L'))
		{
			_Liquid->changeMass(LiquidVALUECHANGER);
		}
		if (draw->keyTyped('i') || draw->keyTyped('I'))
		{
			_Liquid->setDensity(_Ball->getDensity());
		}
	}

	if (_Liquid != NULL && _Ball != NULL)
	{
		inLiquidCheck();
	}
}

void ArkimedesPrincip::onClick(DemoHandler* draw)
{
	MyVector mouseTemp = draw->getMouseLocation();
	if (once)
	{
		_Ball = new Ball(mouseTemp, MyVector(0, 0, 0), _ballCreationMass, _ballCreationRad);
		once = false;
	}
	else
	{
		_Ball->setPos(mouseTemp);
		_Ball->clearVelo();
	}
}

void ArkimedesPrincip::calcBuoyancy()
{
	_Force = getRightVol() * _Liquid->getDensity() * GRAVITY;
	_Ball->addForce(MyVector(0,-_Force,0));
}

void ArkimedesPrincip::inLiquidCheck()
{
	if (_Ball->getPos().mY - _Ball->getRad() < LiquidBORDER)
	{
		calcBuoyancy();
		_Ball->setInLiquid(1);
	}
	else
	{
		_Ball->setInLiquid(0);
		_newVol = 0;
	}
}

float ArkimedesPrincip::getRightVol()
{
	if ((_Ball->getPos().mY + _Ball->getRad()) < LiquidBORDER)
	{
		//Ball is fully submerged
		return _Ball->getVolume();
	}
	else
	{
		//Parts of the ball is submerged
		_newVol = (2.0f / 3.0f)*M_PI*powf((LiquidBORDER - (_Ball->getPos().mY - _Ball->getRad())), 3);
		return _newVol;
	}
}

const string ArkimedesPrincip::getName()
{
	return "Archimedes' principle";
}
const string ArkimedesPrincip::getInfo()
{
	return "Create ball with left mouse button.\nLiquid hotkeys: \"z\", \"x\" inc/dec height | \"c\", \"v\" inc/dec length | \"b\", \"n\" inc/dec width | \n\"k\", \"l\" inc/dec mass | \"i\" to set Liquid density to the same as the Ball(kinda cheaty).\nBall hotkeys: \"d\",\"f\" dec/inc mass | \"a\", \"s\" dec/inc rad | move ball by holding down left\nmouse button.\nCommon hotkeys: press \"r\" to reset. ";
}