#ifndef _MENUSTATE_
#define _MENUSTATE_

#include "State.h"
#include <string>
#include <SFML\System\Time.hpp>

class MenuItems
{
public:
	MenuItems(const std::string &normal, const std::string &hover , sf::Vector2f position);

	void draw(int selected);
private:
	sf::Vector2f mPosition;
	sf::Texture mButton[2];
	//if you want it could be connected with an AssetManager
	//then change sf::Texture *mButton[2]; instead
	//and change the calling for the load of the asset manager
	
};

class MenuState:
	public State
{
public:
	enum MenuOptions
	{
		START = 0,	// 0
		OPTIONS,	// 1
		EXIT,		// 2
		NR_STATES	// 3
	};
	MenuState();
	virtual ~MenuState();

	virtual void update();
	virtual void draw() const;
	virtual void handleEvent(const sf::Event &evt);
private:
	int mState;
	sf::Texture mTitleScreen;
	// same goes here execpt sf::Texture *mTitleScreen;
	sf::View mView;
	sf::Time mTimer;
	MenuItems *mMenuItems[NR_STATES];
};

#endif