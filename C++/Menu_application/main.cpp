#include <SFML\Window\Event.hpp>
#include "StateManager.h"
#include "TimeManager.h"
#include "WindowManager.h"
#include "State.h"
int main()
{
	TimeManager::init();
	WindowManager::init();
	StateManager::init();

	sf::Event evt;

	gTimeManager->reset();

	while(gWindowManager->getWindow().isOpen())
	{
		gTimeManager->resetFrameTimeClock();
		gWindowManager->pollEvent(evt);
		if(evt.type == sf::Event::Closed)
			gWindowManager->closeWindow();

		State* activeState = gStateManager->getActiveState();
		activeState->handleEvent(evt);
		
		gWindowManager->clearWindow(sf::Color::Black);
		if(activeState)
		{
			activeState->update();
			activeState->draw();
		}
		gWindowManager->updateDisplay();
	}

	StateManager::deInit();
	WindowManager::deInit();
	TimeManager::deInit();

	return EXIT_SUCCESS;
}