#include "StateManager.h"
#include "TimeManager.h"
#include "State.h"
#include "GameState.h"
#include "MenuState.h"
#include "OptionsState.h"

StateManager *gStateManager;

void StateManager::init()
{
	gStateManager = new StateManager();
}

void StateManager::deInit()
{
	delete gStateManager;
}

void StateManager::update()
{
	switch(mRestartState)
	{
	case GAME:
		delete mState[mRestartState];
		mState[mRestartState] = new GameState();
		mActiveState = mState.at(mRestartState);
		gTimeManager->reset();
		break;
	case MENU:
		delete mState[MENU];
		mState[MENU] = new MenuState();
		mActiveState = mState.at(mRestartState);
		gTimeManager->reset();
		break;
	case OPTION:
		delete mState[OPTION];
		mState[OPTION] = new OptionsState();
		mActiveState = mState.at(mRestartState);
		gTimeManager->reset();
		break;

	};
	mRestartState = NONE;
}

void StateManager::stateRestart(activeState state)
{
	mRestartState = state;
}

void StateManager::setActiveState(activeState state)
{
	mActiveState = mState.at(state);
}

State* StateManager::getActiveState() const
{
	return mActiveState;
}

StateManager::StateManager() : 
				mRestartState(NONE)
{
	mState[MENU] =		new MenuState();
	mState[GAME] =		new GameState();
	mState[OPTION] =	new OptionsState();
	mActiveState =		mState.at(MENU);
}

StateManager::~StateManager() {}