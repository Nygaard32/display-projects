#ifndef _GAMESTATE_
#define _GAMESTATE_

#include "State.h"
/*Not needed just for demo*/
#include <SFML\Graphics\CircleShape.hpp>

class GameState:
	public State
{
public:
	GameState();
	virtual ~GameState();

	virtual void update();
	virtual void draw() const;
	virtual void handleEvent(const sf::Event &evt);
private:
	sf::View mView;
	sf::Texture mGameScreen;

	sf::Sprite mPlayer;
	sf::Texture mPlayerText;
	//if you want it could be connected with an AssetManager
	//then change sf::Texture *mTexture; instead
	//and change the calling for the load of the asset manager

	sf::CircleShape mCircel;
	//for test
	
};

#endif