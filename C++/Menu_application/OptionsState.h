#ifndef _OPTIONSSTATE_
#define _OPTIONSSTATE_

#include "State.h"

class OptionItems
{
public:
	OptionItems(const std::string &normal, const std::string &hover , sf::Vector2f position);

	void draw(int selected);
private:
	sf::Vector2f mPosition;
	sf::Texture mButton[2];
};

class OptionsState:
	public State
{
public:
	//Option to add more buttons
	enum OptionTypes
	{
		MENU = 0,
		EXIT,
		NR_STATES
	};

	OptionsState();
	virtual ~OptionsState();

	virtual void update();
	virtual void draw() const;
	virtual void handleEvent(const sf::Event &evt);
private:
	int mState;
	sf::Texture mOptionScreen;
	// same goes here execpt sf::Texture *mTitleScreen;
	sf::View mView;
	sf::Time mTimer;
	OptionItems *mOptionItems[NR_STATES];
};

#endif