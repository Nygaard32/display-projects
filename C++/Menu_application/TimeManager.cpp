#include "TimeManager.h"
#include <iostream>

TimeManager *gTimeManager = 0;

void TimeManager::init()
{
	if(gTimeManager == 0)
		gTimeManager = new TimeManager();
}

void TimeManager::deInit()
{
	delete gTimeManager;
	gTimeManager = 0;
}

TimeManager::TimeManager()
{
	resetFrameTimeClock();
}

TimeManager::~TimeManager()
{
}

void TimeManager::reset()
{
	mFrameTime = sf::Time::Zero;
	mFrameTimeClock.restart();
}

void TimeManager::resetFrameTimeClock()
{
	mFrameTime = mFrameTimeClock.getElapsedTime();
	mFrameTimeClock.restart();
}

const sf::Time& TimeManager::getFrameTime() const
{
	return mFrameTime;
}

sf::Time TimeManager::getProgramAge() const
{
	return mProgramAge.getElapsedTime();
}