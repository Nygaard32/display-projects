#ifndef _STATE_
#define _STATE_

#include <SFML\Graphics\Sprite.hpp>
#include <SFML\Graphics\Texture.hpp>
#include <SFML\Window\Event.hpp>
#include <SFML\Graphics\View.hpp>
#include <SFML\System\Time.hpp>

class State
{
public:
	State();
	virtual ~State();

	virtual void update() = 0;
	virtual void draw() const = 0;
	virtual void handleEvent(const sf::Event &evt) = 0;
};
#endif