#ifndef _WINDOWMANAGER_
#define _WINDOWMANAGER_

#include <SFML/Graphics/RenderWindow.hpp>


class WindowManager
{
public:
	static void init();
	static void deInit();

	void draw(const sf::Drawable &drawable, const sf::Shader *shader =0);
	bool pollEvent(sf::Event &evnt);
	void closeWindow();
	void updateDisplay();
	void clearWindow(sf::Color color = sf::Color::Black);
	void setView(const sf::View &view);
	const sf::View &getView() const;
	sf::RenderWindow &getWindow();

private:
	WindowManager();
	~WindowManager();
	WindowManager(const WindowManager&);
	WindowManager& operator=(const WindowManager&);

	sf::RenderWindow mWindow;

	short mTopCoord;
	short mLeftCoord;
};

#endif

extern WindowManager *gWindowManager;