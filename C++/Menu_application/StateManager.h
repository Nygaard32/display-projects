#ifndef _STATEMANAGER_
#define _STATEMANAGER_

#include <map>
#include <SFML\Graphics\RenderWindow.hpp>

class State;

class StateManager
{
public:
	enum activeState
	{
		NONE,
		MENU,
		GAME,
		OPTION
	};

	static void		init();
	static void		deInit();

	void			update();
	void			stateRestart(activeState state);
	void			setActiveState(activeState state);
	State*			getActiveState() const;
private:
	StateManager();
	~StateManager();
	StateManager(const StateManager&);
	bool& operator=(const StateManager&);

	activeState mRestartState;
	State* mActiveState;
	std::map<activeState, State*> mState;

};
#endif

extern StateManager *gStateManager;