#include "WindowManager.h"

WindowManager *gWindowManager;

void WindowManager::init()
{
	gWindowManager = new WindowManager();
}

void WindowManager::deInit()
{
	delete gWindowManager;
}

void WindowManager::draw(const sf::Drawable &drawable, const sf::Shader *shader)
{
	if(shader)
	{
		sf::View tempView = getView();
		mWindow.setView(sf::View(sf::FloatRect(mLeftCoord,mTopCoord, mWindow.getSize().x, mWindow.getSize().y)));
		sf::RenderStates tempoRender;
		tempoRender.shader = shader;
		mWindow.draw(drawable, shader);
		mWindow.setView(tempView);
	}
	else
	{
		mWindow.draw(drawable);
	}
}

bool WindowManager::pollEvent(sf::Event &evnt)
{
	return mWindow.pollEvent(evnt);
}

void WindowManager::closeWindow()
{
	mWindow.close();
}

void WindowManager::updateDisplay()
{
	mWindow.display();
}

void WindowManager::clearWindow(sf::Color color)
{
	mWindow.clear(color);
}

void WindowManager::setView(const sf::View &view)
{
	mWindow.setView(view);
}

const sf::View &WindowManager::getView() const
{
	return mWindow.getView();
}

sf::RenderWindow &WindowManager::getWindow()
{
	return mWindow;
}

WindowManager::WindowManager():
	mWindow(sf::VideoMode(1280,720,32),"MenuSystem")
{
	mWindow.setFramerateLimit(60);
	mWindow.setVerticalSyncEnabled(true);
	mTopCoord = 0;
	mLeftCoord = 0;
}

WindowManager::~WindowManager() {}