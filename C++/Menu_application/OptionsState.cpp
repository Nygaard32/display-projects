#include "OptionsState.h"
#include "WindowManager.h"
#include "TimeManager.h"
#include "StateManager.h"

OptionItems::OptionItems(const std::string &normal, const std::string &hover, sf::Vector2f position) :
	mPosition(position)
{
	mButton[0].loadFromFile(normal);
	mButton[1].loadFromFile(hover);
}

void OptionItems::draw(int selected)
{
	sf::Sprite tempSprite(mButton[selected]);
	tempSprite.setPosition(mPosition);
	gWindowManager->draw(tempSprite);
}

OptionsState::OptionsState()
{
	mOptionScreen.loadFromFile("resources/Option.png");
	mView = gWindowManager->getWindow().getDefaultView();
	mView.setSize(sf::Vector2f(mOptionScreen.getSize()));
	mView.setCenter(sf::Vector2f(mOptionScreen.getSize()) * 0.5f);
	mOptionItems[MENU] = new OptionItems("resources/MenuNormal.png", "resources/MenuHover.png", sf::Vector2f(800,100));
	mOptionItems[EXIT] = new OptionItems("resources/ExitNormal.png", "resources/ExitHover.png", sf::Vector2f(800,300));
}

OptionsState::~OptionsState() {}

void OptionsState::update()
{
	sf::Time tempTime = gTimeManager->getProgramAge();

	static bool upKey = false;
	static bool downKey = false;

	bool pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Up);
	if(!upKey && pressed)
	{
		upKey = true;
		mTimer = gTimeManager->getProgramAge();
		--mState;
	}
	else if(pressed && (tempTime - mTimer).asSeconds() > 0.2)
	{
		mTimer = gTimeManager->getProgramAge();
		--mState;
	}
	else if(!pressed)
		upKey = false;
	
	if(mState < 0)
	mState = EXIT;

	pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Down);
	
	if(!downKey && pressed)
	{
		downKey = true;
		mTimer = gTimeManager->getProgramAge();
		++mState;
	}
	else if(pressed && (tempTime - mTimer).asSeconds() > 0.2)
	{
		mTimer = gTimeManager->getProgramAge();
		++mState;
	}
	else if(!downKey)
		downKey = false;
	if(mState > EXIT)
		mState = MENU;
}

void OptionsState::draw() const
{
	gWindowManager->setView(mView);
	sf::Sprite tempSprite(mOptionScreen);
	gWindowManager->draw(tempSprite);

	for	(int i = 0; i < NR_STATES; ++i)
	{
		mOptionItems[i]->draw(i == mState);
	}
}

void OptionsState::handleEvent(const sf::Event &evt)
{
	if(evt.type == sf::Event::EventType::KeyPressed)
	{
		if(evt.key.code == sf::Keyboard::Return)
		{
			switch(mState)
			{
			case MENU:
				gStateManager->setActiveState(StateManager::MENU);
				break;
			case EXIT:
				gWindowManager->closeWindow();
				break;

			}

		}
	}
}
