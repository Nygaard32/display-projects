#include "MenuState.h"
#include "WindowManager.h"
#include "TimeManager.h"
#include "StateManager.h"

MenuItems::MenuItems(const std::string &normal, const std::string &hover, sf::Vector2f position) :
	mPosition(position)
{
	mButton[0].loadFromFile(normal);
	mButton[1].loadFromFile(hover);
}

void MenuItems::draw(int selected)
{
	sf::Sprite tempSprite(mButton[selected]);
	tempSprite.setPosition(mPosition);
	gWindowManager->draw(tempSprite);
}

MenuState::MenuState() :
mState(0)
{
	mTitleScreen.loadFromFile("resources/Menu.png");
	mView = gWindowManager->getWindow().getDefaultView();
	mView.setSize(sf::Vector2f(mTitleScreen.getSize()));
	mView.setCenter(sf::Vector2f(mTitleScreen.getSize())*0.5f);
	mMenuItems[START] = new MenuItems("resources/StartNormal.png", "resources/StartHover.png", sf::Vector2f(800,100));
	mMenuItems[OPTIONS] = new MenuItems("resources/OptionNormal.png", "resources/OptionHover.png", sf::Vector2f(800,300));
	mMenuItems[EXIT] = new MenuItems("resources/ExitNormal.png", "resources/ExitHover.png", sf::Vector2f(800,500));
}

MenuState::~MenuState() {}

void MenuState::update()
{
	sf::Time tempTime = gTimeManager->getProgramAge();

	static bool upKey = false;
	static bool downKey = false;

	bool pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Up);
	if(!upKey && pressed)
	{
		upKey = true;
		mTimer = gTimeManager->getProgramAge();
		--mState;
	}
	else if(pressed && (tempTime - mTimer).asSeconds() > 0.2)
	{
		mTimer = gTimeManager->getProgramAge();
		--mState;
	}
	else if(!pressed)
		upKey = false;
	
	if(mState < 0)
	mState = EXIT;
	pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Down);
	
	if(!downKey && pressed)
	{
		downKey = true;
		mTimer = gTimeManager->getProgramAge();
		++mState;
	}
	else if(pressed && (tempTime - mTimer).asSeconds() > 0.2)
	{
		mTimer = gTimeManager->getProgramAge();
		++mState;
	}
	else if(!downKey)
		downKey = false;
	if(mState > EXIT)
		mState = START;
}

void MenuState::draw() const
{
	gWindowManager->setView(mView);
	sf::Sprite tempSprite(mTitleScreen);
	gWindowManager->draw(tempSprite);

	for	(int i = 0; i < NR_STATES; i++)
	{
		mMenuItems[i]->draw(i == mState);
	}
}

void MenuState::handleEvent(const sf::Event &evt)
{
	if(evt.type == sf::Event::EventType::KeyPressed)
	{
		if(evt.key.code == sf::Keyboard::Return)
		{
			switch(mState)
			{
			case START:
				gStateManager->setActiveState(StateManager::GAME);
				break;
			case OPTIONS:
				gStateManager->setActiveState(StateManager::OPTION);
				break;
			case EXIT:
				gWindowManager->closeWindow();
				break;

			}

		}
	}
}