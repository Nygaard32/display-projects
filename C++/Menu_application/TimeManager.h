#ifndef _TIMEMANAGER_
#define _TIMEMANAGER_

#include <SFML\System\Clock.hpp>

class TimeManager
{
public:
	static void init();
	static void deInit();

	void				reset();
	void				resetFrameTimeClock();
	const sf::Time&		getFrameTime() const;
	sf::Time			getProgramAge() const;
private:
	TimeManager();
	~TimeManager();
	TimeManager(const TimeManager&);
	TimeManager& operator=(const TimeManager&);

	sf::Clock mProgramAge;
	sf::Clock mFrameTimeClock;
	sf::Time mFrameTime;
};

#endif

extern TimeManager *gTimeManager;