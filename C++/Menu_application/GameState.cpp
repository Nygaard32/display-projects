#include "GameState.h"
#include "StateManager.h"
#include "WindowManager.h"

GameState::GameState()
{
	mGameScreen.loadFromFile("resources/Game.png");


	mPlayerText.loadFromFile("resources/Player.png");
	mPlayer.setTexture(mPlayerText);

	mCircel.setRadius(32);
	mCircel.setFillColor(sf::Color::Blue);
	mCircel.setOutlineColor(sf::Color::Black);

	mView = gWindowManager->getWindow().getDefaultView();
	mView.setSize(sf::Vector2f(mGameScreen.getSize()));
	mView.setCenter(sf::Vector2f(mGameScreen.getSize())*0.5f);
}

GameState::~GameState() {}

void GameState::update()
{
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		mPlayer.move(-5,0);
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		mPlayer.move(5,0);
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		mPlayer.move(0,-5);
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		mPlayer.move(0,5);

	mCircel.move(5,0);
}

void GameState::draw() const
{
	gWindowManager->setView(mView);

	sf::Sprite tempBackground(mGameScreen);

	
	gWindowManager->draw(tempBackground);
	gWindowManager->draw(mPlayer);
	gWindowManager->draw(mCircel);
}

void GameState::handleEvent(const sf::Event &evt)
{
	if(evt.type == sf::Event::EventType::KeyPressed)
	{
		if(evt.key.code == sf::Keyboard::Escape)
		{
			gStateManager->setActiveState(StateManager::MENU);
		}
	}
}