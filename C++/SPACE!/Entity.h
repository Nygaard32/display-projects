#ifndef INCLUDE_ENTITY
#define INCLUDE_ENTITY

#include "VGCVirtualGameConsole.h"

class Entity
{
public:
	enum Category{FRIENDLY,ENEMY};  /*Ger bullets olika typer att den �r fiendens eller spelarens*/
	enum Layer{BACKGROUND};	/*F�r att kunna h�lla koll vad som skall ritas och var en explosion skall ritas*/
	typedef std::vector<Entity*> Entityvector;	/*Skapar en vector f�r att inneh�lla bilder*/
	
	Entity();	/*konstuktor*/
	virtual ~Entity();	/*destrukot*/
	virtual VGCVector getPosition()=0;  /*Skaffar fram positionen p� objektet*/
	virtual void render(Layer layer)=0;		/*Ritar ut sakerna*/
	virtual bool isAlive()=0;			/*Checkar om fienden �r vid liv eller spelaren*/
	virtual void update(Entityvector &entities)=0;	/*updaterar game objekten*/
	virtual int collide(Entity *entity, Entityvector &entities)=0;	/*H�ller k�ll p� om skeppen koliderar*/
	virtual Category getCategory() = 0;  /*H�mtar category f�r att kunnas anv�nda i window*/
	virtual int getDmg()=0; /*S�tter ut dmg v�rden p� bullets(5) och kollision(10), kollisionExplo(0)*/
	virtual int getRadius()=0; /*G�r det m�jligt att ber�kna kollision eller inte (dx^2 + dy^2 < (r0 + r1 )^2 )*/ 
	
};

#endif