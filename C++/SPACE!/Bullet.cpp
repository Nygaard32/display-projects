#include "Bullet.h"

static VGCImage BulletImg;
static const int RADIUS = 4;
static const int DAMAGE = 5;
static const int SPEED = 5;
static const int SCORE = 0;


Bullet::Bullet(Entity::Category category, VGCVector &position, VGCVector &direction):
	Entity(),
	mPosition(position),
	mDirection(direction),
	mCategory(category),
	mIsAlive(true){
}

	Bullet::Category Bullet::getCategory()
	{
		return mCategory;
	}
	int Bullet::getRadius()
	{
		return RADIUS;
	}
	void Bullet::render(Layer layer)
	{
		if(layer == BACKGROUND)
		{
			const VGCVector frameIndex(0, 0);
			const VGCAdjustment adjustment(0.5, 0.5);
			VGCDisplay::renderImage(BulletImg, frameIndex, mPosition, adjustment);
		}
	}
	int Bullet::getDmg()
	{
			return DAMAGE;
	}
	int Bullet::collide(Entity *entity, Entityvector &entities)
	{
		if(10 < entity->getDmg())
		{
			mIsAlive = false;	/*S�tter att den �r d�d*/
			return SCORE;
		}
		else{
			return 0; /*Om det inte st�mmer s� h�nder igent*/
		}
	}
	bool Bullet::isAlive()
	{
		return mIsAlive;
	}
	void Bullet::initialize()
	{
		const std::string Shot = "Bullet.png";
		const int X_FRAME_COUNT = 1;
		const int Y_FRAME_COUNT = 1;
		BulletImg = VGCDisplay::openImage(Shot, X_FRAME_COUNT, Y_FRAME_COUNT);
	}
	void Bullet::finalize()
	{
		VGCDisplay::closeImage(BulletImg);
	}
	void Bullet::update(Entityvector &entities)
	{
		movement();
	}
	VGCVector Bullet::getPosition()
	{
	return mPosition;
	}
	void Bullet::movement()
	{
		mPosition += SPEED * mDirection;

		int x = mPosition.getX();
		int y = mPosition.getY();

		if(y < -20  || x > 430 ||  x < 0 || y > 530 || x > 430 ||  x < 0)
		{
			mIsAlive = false;
		}
	}
	Bullet::~Bullet()
	{

	}