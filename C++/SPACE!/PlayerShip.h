#ifndef INCLUDE_PLAYERSHIP
#define INCLUDE_PLAYERSHIP

#include "Entity.h"
#include "Bullet.h"

class PlayerShip :
	public Entity
{
public:
	PlayerShip(VGCVector &position);
	virtual ~PlayerShip();
	virtual bool isAlive();
	virtual void update(Entityvector &entities);
	virtual Category getCategory();
	virtual void render(Layer layer);
	static void finalize();
	virtual VGCVector getPosition();
	virtual int getRadius();
	virtual int getLife();
	static void initialize();
	virtual int getDmg();
	virtual int collide(Entity *entity, Entityvector &entities);
	
private:
	void movement();
	bool mIsAlive;
	VGCVector mPosition;
	VGCVector mDirection;
	int mReloadTimer;
	void fire(Entityvector &entities);
		
};

#endif