#include "PlayerShip.h"

static VGCImage Ship;
static const int SPEED = 5;
static const int RADIUS = 16;
static int LIFE = 100;
static int RELOAD = 0;
static int DAMAGE = 1;
PlayerShip::PlayerShip(VGCVector &position):
Entity(),
	mIsAlive(true),
	mPosition(position),
	mReloadTimer(RELOAD){
}

PlayerShip::~PlayerShip()
{
	//VGCClock::closeTimer(mReloadTimer);
}

bool PlayerShip::isAlive()
{
	if(0 < LIFE)
	{
		mIsAlive = true;
	}
	else
	{
		mIsAlive = false;
		LIFE = 0;
	}

		return mIsAlive;
}

PlayerShip::Category PlayerShip::getCategory()
{
	return FRIENDLY;
}

VGCVector PlayerShip::getPosition()
{
	return mPosition;
}

int PlayerShip::getRadius()
{
	return RADIUS;
}

int PlayerShip::getLife()
{
	return LIFE;
}

int PlayerShip::getDmg()
{
	return DAMAGE;
}

int PlayerShip::collide(Entity *entity, Entityvector &entities)
{
	LIFE -= entity->getDmg();
	if(LIFE < entity->getDmg())
	{
		mIsAlive = false;	/*S�tter att den �r d�d*/	
		return 0;
	}
	else{
		return 0; /*Om det inte st�mmer s� h�nder igent*/
	}
	return 0;
}

void PlayerShip::update(Entityvector &entities)
{
	movement();
	if(VGCKeyboard::isPressed(VGCKey::SPACE_KEY))
	{
	fire(entities);
	}
}

void PlayerShip::fire(Entityvector &entities)
{
	if(mReloadTimer == 5)
	{
		for(int i = -1; i < 2; i++){
			VGCVector mDirection(0,-1);
			entities.push_back(new Bullet(FRIENDLY, mPosition,mDirection + VGCVector(i,0)));
		}
		mReloadTimer = 0;
	}
	else{
		mReloadTimer++;
	}
}

void PlayerShip::render(Layer layer)
{
	if(layer == BACKGROUND)
	{
		const VGCVector frameIndex(0, 0);
		const VGCAdjustment adjustment(0.5, 0.5);
		VGCDisplay::renderImage(Ship, frameIndex, mPosition, adjustment);
	}
}

void PlayerShip::initialize()
{
	const std::string Shipperson = "Ship.png";
	const int X_FRAME_COUNT = 1;
	const int Y_FRAME_COUNT = 1;
	Ship = VGCDisplay::openImage(Shipperson, X_FRAME_COUNT, Y_FRAME_COUNT);
}

void PlayerShip::finalize()
{
	VGCDisplay::closeImage(Ship);
}
/*Spelarens f�rflyttning*/
void PlayerShip::movement()
{
	int y = mPosition.getY();
	int x = mPosition.getX();
	

	const int MIN_X = RADIUS;
	const int MAX_X = VGCDisplay::getWidth() - RADIUS;
	const int MIN_Y = RADIUS;
	const int MAX_Y = VGCDisplay::getHeight() - RADIUS;

	if(!VGCKeyboard::isPressed(VGCKey::ARROW_UP_KEY))
	{
		y += SPEED;

		if(y > MAX_Y)
		{
			y =  MAX_Y;
		}
	}
	if(!VGCKeyboard::isPressed(VGCKey::ARROW_DOWN_KEY))
	{
		y -= SPEED;
	
		if(y < MIN_Y)
		{
			y =  MIN_Y;
		}
	}
	if(!VGCKeyboard::isPressed(VGCKey::ARROW_LEFT_KEY))
	{
		x += SPEED;

		if(x > MAX_X)
		{
			x =  MAX_X;
		}
	}
	if(!VGCKeyboard::isPressed(VGCKey::ARROW_RIGHT_KEY))
	{
		x -= SPEED;

		if(x < MIN_X)
		{
			x =  MIN_X;
		}		
	}

	mPosition.setY(y);
	mPosition.setX(x);
}
