#ifndef INCLUDE_ENEMYSHIP
#define INCLUDE_ENEMYSHIP

#include "Entity.h"
#include "Bullet.h"

class EnemyShip :
	public Entity
{
public:
	EnemyShip(const VGCVector &position, const VGCVector &direction); /*konstuktor g�r tv� nya constanta vectorer som �r referencer till position och direction*/
	virtual ~EnemyShip();
	virtual bool isAlive();	/*S� att man kan kontrollera om en fiende �r vid liv*/
	virtual void update(Entityvector &entities); /**/
	virtual Category getCategory();			/*S�tter g�r s� att enum f�rst�r att detta �r ENEMY*/
	virtual void render(Layer layer);		/*Ritar ut bilden*/
	static void finalize();		/*St�nger bilden*/
	virtual VGCVector getPosition(); /*H�mtar position av skeppet*/
	virtual int getRadius();	/*F�r tag i omkretsen till fientliga skeppet*/
	virtual int getDmg();		/*H�mtar vilken skada man tar*/
	virtual int collide(Entity *entity, Entityvector &entities); /*En funktion som har hand om det blir en kollision	*/
	static void initialize();	/*�ppnar en bild/bilder*/
	
private:
	void moveRando();		/*En private void som har hand om r�reslen f�r skeppen*/
	bool mIsAlive;			/*En egen funktion som h�ller k�ll om fienden �r vid liv*/
	VGCVector mPosition;	/*En medlems funktion som tar reda p� vart skeppet ligger i sk�rmen*/
	VGCVector mDirection;	/*En medlems funktion som har hand om vilket h�ll skeppet skall r�ra sig mot*/
	int mDeathTimer;
	int mReloadTimer;	/*En medlems funktion som har hand om timern*/
	void fire(Entityvector &entities); /*Har hand om fire p� skeppet*/
	void DeathFade();
};

#endif