#ifndef INCLUDE_BULLET
#define INCLUDE_BULLET

#include "Entity.h"

class Bullet :
	public Entity
{
public:
	Bullet(Entity::Category category, VGCVector &position, VGCVector &direction);
	virtual ~Bullet();
	virtual VGCVector getPosition();  /*Skaffar fram positionen p� objektet*/
	virtual void render(Layer layer);		/*Ritar ut sakerna*/
	virtual bool isAlive();			/*Checkar om fienden �r vid liv eller spelaren*/
	virtual void update(Entityvector &entities);	/*updaterar game objekten*/
	virtual int collide(Entity *entity, Entityvector &entities);	/*H�ller k�ll p� om skeppen koliderar*/
	virtual Category getCategory();  /*H�mtar category f�r att kunnas anv�nda i window*/
	virtual int getDmg(); /*S�tter ut dmg v�rden p� bullets(5) och kollision(10), kollisionExplo(0)*/
	virtual int getRadius();
	static void initialize();
	static void finalize();
private:
	void movement();
	VGCVector mPosition;
	VGCVector mDirection;
	Entity::Category mCategory;
	bool mIsAlive;
};

#endif