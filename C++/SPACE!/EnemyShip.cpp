#include "EnemyShip.h"

/*BUU H�RD KODAT*/

static VGCImage Melon;			/*En VGCImage som lagrar en speciell bild under namnet Melon*/
static VGCImage KABBOOMM;
static const int SPEED = 2;		/*Hastigheten den skall r�ra sig i x-, y-led*/
static const int RADIUS = 16; /*Bildens radius*/
static const int RELOAD = 0; /*Tids intervallen fienderna skjuter p�*/
static const int FADE = 0;
static const int DAMAGE = 10; /*Om man kolliderar med fienden s� tar man 10 dmg*/
static const int SCORE = 100;
static const int SIZE = 32;

EnemyShip::EnemyShip(const VGCVector &position, const VGCVector &direction):
	Entity(),
	mIsAlive(true),	
	mReloadTimer(RELOAD),
	mPosition(position),
	mDirection(direction),
	mDeathTimer(FADE){
}
EnemyShip::~EnemyShip()
{

}
/*Retunerar v�rdet till mIsAlive av isAlive*/
bool EnemyShip::isAlive()
{
	return mIsAlive;
}
/*Retunerar enemy s� att EnemyShip blir f�rst�t som en fiende f�r programmet*/
EnemyShip::Category EnemyShip::getCategory()
{
	return ENEMY;
}
/*Skaffar fram en position p� skeppet och retunerar v�rdet till mPosition*/
VGCVector EnemyShip::getPosition()
{
	return	mPosition;		
}
/*Retunerar v�der RADIUS till getRadius*/
int EnemyShip::getRadius()
{
	return RADIUS;		
}
/*Returnerar DAMAGE v�rde till getDmg*/
int EnemyShip::getDmg()
{
	return DAMAGE;
}
/*Retunerar en explosion till positionen en fiende var p� och ger en scroe till spelaren om den har kolliderat med n�gon*/
int EnemyShip::collide(Entity *entity, Entityvector &entities)
{
	if(0 < entity->getDmg())
	{
		DeathFade();
		mIsAlive = false;	/*S�tter att den �r d�d*/			
		return SCORE; /*Ger spelaren po�ng f�r att ha d�dat fienden*/
	}
	else{
		return 0; /*Om det inte st�mmer s� h�nder igent*/
	}
}
/*Updaterar fiendens movement och fire*/
void EnemyShip::update(Entityvector &entities)
{
	moveRando();/*Anropar moveRando, s� att skeppet kan r�ra p� sig*/
	fire(entities);  /*Anropar fire funktionen f�r executa den*/
}
/*Ritar ut bilden*/
void EnemyShip::render(Layer layer)
{
	if(layer == BACKGROUND){
		const VGCVector frameIndex(0, 0);
		const VGCAdjustment adjustment(0.5, 0.5);
			if(mIsAlive){
				VGCDisplay::renderImage(Melon, frameIndex, mPosition, adjustment);
				}
			else{
					if(mDeathTimer == 50000)
					{
						mDeathTimer = 0;
					}
					else
					{
						mDeathTimer++;
					}
					VGCDisplay::renderImage(KABBOOMM, frameIndex, mPosition, adjustment);
		}
		}
		
}
/*�ppnar bilden*/
void EnemyShip::initialize()
{
	const std::string filename = "Melon.png";	/*Filename inneh�ller nu bara bilden Melon*/
	const int X_FRAME_COUNT	= 1;				/*Den har bara en fram(en ruta) x-led*/
	const int Y_FRAME_COUNT = 1;				/*Inneh�ller en fram i y-led*/
	Melon = VGCDisplay::openImage(filename, X_FRAME_COUNT, Y_FRAME_COUNT); /*Melon blir tilldelad melon.png och den animationer*/

	const std::string Explosion = "Explosion.png";
	const int X2_FRAME_COUNT = 1;
	const int Y2_FRAME_COUNT = 1;
	KABBOOMM = VGCDisplay::openImage(Explosion, X2_FRAME_COUNT, Y2_FRAME_COUNT);

}
/*St�nger bilden efter den har �ppnats*/
void EnemyShip::finalize() 
{
	VGCDisplay::closeImage(Melon);
	VGCDisplay::closeImage(KABBOOMM);
}
/*Ger en movement till enemyShip*/
void EnemyShip::moveRando()
{
	mPosition += SPEED * mDirection; 
	int x = mPosition.getX();	/*G�r s� att x �r lika med den nu varande positionen i x-led*/
	int y = mPosition.getY();	/*G�r s� att y �r lika med den nu varande positionen i y-ed*/
	const int MIN_X = RADIUS;	/*Skapar en constant int som h�ller v�rdet av omkretesen till skeppet. Denna tar reda p� var v�gen ligger som inte invader skeppet skall kunna passera */
	const int MAX_X = VGCDisplay::getWidth() - RADIUS;	/* SKapar en konstan som h�mtar displayens bredd och sedan minskar den med omkretsen p� skeppet
														f�r att f� fram vart det h�gsta x ligger p� som den kan visas p�, utan att g� utan f�r sk�rmen*/
	if(x < MIN_X) /*Om x �r minde �n MIN_X s� byter den riktning mot MAX_X*/
	{
		x =  MIN_X;
		mDirection.setX(-mDirection.getX()); /*Ger en hastighet som g�r att den r�r sig mot MAX_X*/
	}
	if(MAX_X < x) /*Om x �r mindre �n MAX_X s� byter den riktning mot MIN_X*/
	{
		x = MAX_X;
		mDirection.setX(-mDirection.getX()); /*Ger en hastighet som g�r att den r�r sig mot MAX_X*/
	}
	if(y > VGCDisplay::getHeight()+SIZE)
	{
		mIsAlive = false;
	}
	mPosition.setX(x);  /*S�tter ut nya x v�rden som skeppet skall r�ra sig mot*/
	mPosition.setY(y);	/*S�tter ut nya y v�rden som skeppet skall r�ra sig mot*/

	
}
/*G�r s� att fienden kan skjuta ett skott efter en viss tid*/
void EnemyShip::fire(Entityvector &entities)
{
	if(mReloadTimer==15)
	{
		VGCVector mDirection(0,1);/*Skotter g�r alltid ner�t p� sk�rmen*/
		entities.push_back(new Bullet(ENEMY, mPosition, mDirection)); /*Skapar en ny buller som skeppet skjuter fr�n sin position*/
		mReloadTimer = 0; /*Startar om timern*/
	}
	else{
		mReloadTimer++;
	}
}
void EnemyShip::DeathFade()
{
}
