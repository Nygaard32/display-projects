#include "Window.h"

using namespace std;

static const int FRAMES_PER_SECOND = 60;
static const int EDGE_DELTA = 32;
static const int SPAWN_DELTA = -16;
static const int SPAWN_TIME = 0;
static const VGCColor backgroundColor = VGCColor(255, 0, 0, 0);
static const VGCColor fontColor(255, 255, 0, 0);
static const string fontName("Arial");
static const int FONT_SIZE = 24;
static const int RADIUS = 16;

Window::Window() :
	mSpawnTimer(SPAWN_TIME),
	mFont(VGCDisplay::openFont(fontName, FONT_SIZE)),
	mShip( new PlayerShip(VGCVector(VGCDisplay::getWidth() / 2, 
	VGCDisplay::getHeight() / 2))),
	mEntities(),
	mScore(0)
	{
		mEntities.push_back(mShip);
	}

Window::~Window()
{
	while(!mEntities.empty())
	{
		delete mEntities.back();
		mEntities.pop_back();
	}
}
void Window::run()
{
	VGCTimer timer = VGCClock::openTimer(1.0 / FRAMES_PER_SECOND);
		
		PlayerShip::initialize();
		Bullet::initialize();
		EnemyShip::initialize();

		while(!VGCKeyboard::wasPressed(VGCKey::ESCAPE_KEY) && 0 < mShip -> getLife() && VGCVirtualGameConsole::beginLoop())
		{
			VGCClock::reset(timer);
			
			update();
			killDeadEntities();
			
			detectCollisions();

				if(VGCDisplay::beginFrame())
				{
					VGCDisplay::clear(backgroundColor);
					
					spawnEnemy();
					renderBackground();
					renderLife();
					renderScore();

					VGCDisplay::endFrame();
				}

				
				VGCVirtualGameConsole::endLoop();
				VGCKeyboard::clearBuffer();
				VGCClock::wait(timer);
		}
		
		EnemyShip::finalize();
		PlayerShip::finalize();
		Bullet::finalize();

		//closeVector();
		VGCClock::closeTimer(timer);
		VGCDisplay::closeFont(mFont);
		
		VGCVirtualGameConsole::finalize();
		
}

void Window::update()
{
	Entityvector entities(mEntities);
	for(Entityvector::iterator i = entities.begin(); i != entities.end();
		i++)
	{
			Entity *entity = *i;
			entity->update(mEntities);
	}
}

void Window::spawnEnemy()
{
	if(mSpawnTimer == 15)
	{
		int xSpawn = VGCRandomizer::getInt(RADIUS, VGCDisplay::getWidth() - RADIUS);;
		int xDirection = VGCRandomizer::getInt(-2 , 2);
		while(xDirection == 0)
		{
			xDirection = VGCRandomizer::getInt(-2, 2);
		}
		VGCVector pos(xSpawn,SPAWN_DELTA);
		VGCVector direc(xDirection,1);

		EnemyShip *enemies = new EnemyShip(pos,direc);
		mEntities.push_back(enemies);
		mSpawnTimer = 0;
	}
	else{
		mSpawnTimer++;
	}
}

void Window::detectCollisions()
{
	Entityvector entities(mEntities);
	for(Entityvector::size_type i= 0; i<entities.size(); i++)
	{
		Entity *entity0= entities[i];
		for(Entityvector::size_type j = i + 1; j < entities.size(); j++)
		{
			Entity *entity1 = entities[j];
			if(isOverlap(entity0, entity1) && entity0->getCategory() != entity1->getCategory())
			{
				mScore += entity0->collide(entity1, mEntities);
				mScore += entity1->collide(entity0, mEntities);
			}
		}
	}
}

void Window::renderBackground()
{
	for(Entityvector::iterator i = mEntities.begin(); i != mEntities.end(); i++)
	{
		Entity *entity = *i;
		entity->render(Entity::BACKGROUND);
	}
}

void Window::renderLife()
{
	ostringstream output;
	output << "Life: " << mShip->getLife();
	const string text = output.str();
	const VGCVector position(0, 0);
	const VGCAdjustment adjustment(0.0, 0.0);
	VGCDisplay::renderString(mFont, text, fontColor, position, adjustment);
}

void Window::renderScore()
{
	ostringstream output;
	output << "Score: " << mScore;
	const string text = output.str();
	const VGCVector position(VGCDisplay::getWidth(), 0);
	const VGCAdjustment adjustment(1.0, 0.0);
	VGCDisplay::renderString(mFont, text, fontColor, position, adjustment);
}

void Window::killDeadEntities()
{
	Entityvector entities;
	for(Entityvector::iterator i= mEntities.begin(); i != mEntities.end(); i++)
	{
		Entity *entity = *i;
		if(entity->isAlive())
		{
			entities.push_back(entity);
		}
		else
		{
			
			delete entity;
		}
	}
	mEntities = entities;
}

bool Window::isOverlap(Entity *entity0, Entity *entity1)
{
	const VGCVector position0 = entity0->getPosition();
	const int X0 = position0.getX();
	const int Y0 = position0.getY();
	const int R0 = entity0->getRadius();
	
	const VGCVector position1 = entity1->getPosition();
	const int X1 = position1.getX();
	const int Y1 = position1.getY();
	const int R1 = entity1->getRadius();

	const int DX = X0 - X1;
	const int DY = Y0 - Y1;
	return DX * DX + DY * DY < (R0 + R1) * (R0 + R1);
}