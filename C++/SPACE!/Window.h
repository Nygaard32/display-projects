#ifndef INCLUDE_WINDOW
#define INCLUDE_WINDOW


#include "EnemyShip.h"
#include "PlayerShip.h"
#include <string>
#include <sstream>

class Window
{
public:
	typedef std::vector<Entity*> Entityvector; /*Skapar en ny vector som h�mtar som perkar p� Entity*/
	Window();		/*Konstruktor f�r Windows*/
	~Window();		/*Destruktorn f�r Windows*/
	void run();		/*En void som har hand om att starta f�nstret*/
private: 
	void spawnEnemy();	/*En funktion som skall rita ut nya fiender efter en viss tid har passerat*/
	void update();		/*Funktion som har hand om att updatera spel objekten*/
	void detectCollisions();	/*En funktion som ska anv�ndas n�r tv� objekt �verlappar varandra*/
	void renderBackground();	
	void renderLife();		/*Skriver ut hur mycket liv spelarnens skepp har*/
	void renderScore();		/*Skriver ut hur mycket po�ng spelaren har och updateras n�r spelaren har d�dat en fiende*/
	void killDeadEntities();	/*Tar bort objekt som har blivit d�dad av spelaren*/
	static bool isOverlap(Entity *entity0, Entity *entity1);	/*En funktion som har hand om att identifiera om tv� objekt �verlappar varandra*/
	int mSpawnTimer;	/*En timer som har hand om att rita ut nya fiender efter en viss tid*/	
	VGCFont mFont;			/*Ett teckensnitt som anv�nds d�r man skriver ut po�ng och liv*/	
	PlayerShip *mShip;		/**/
	Entityvector mEntities;	/**/
	int mScore;				/*Tar hand om score ut r�kningen och skrivs ut till sk�rmen efter �t*/
};

#endif