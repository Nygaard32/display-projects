#include "Window.h"

int VGCMain(const VGCStringVector &arguments)
{
	const std::string applicationName = "Hello world";
		const int DISPLAY_WIDTH = 430;
		const int DISPLAY_HEIGHT = 530;

		VGCVirtualGameConsole::initialize(applicationName, DISPLAY_WIDTH, DISPLAY_HEIGHT);

		Window *run = new Window;
		run->run();
		delete run;

		return 0;
}