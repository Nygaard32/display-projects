#include "Player.h"
#include "Animation.h"
#include "SoundManager.h"
#include "Window_Manager.h"
#include <iostream>
#include <SFML\Graphics\ConvexShape.hpp>

Player::Player():
	mVelocity(0,0),
	mMaxVelocity(7),
	mMaxGravity(20.f),
	mPullspeed(15),
	mRange(550),
	mIsAlive(true),
	mHitBox(mPosition.x,mPosition.y,63,114),
	mPosition(128,64)
	{
	
	/*Erik �s�n*/
	mPlay[RIGHT][RUN] = new Animation("Player/Talabu_Running_Right_Unarmed_Spritesheet.png",55,8,false);
	mPlay[RIGHT][IDLE] = new Animation("Player/Talabu_Idle_Right_Unarmed_Spritesheet.png",55,8,false);
	mPlay[RIGHT][REPAIR] = new Animation("Player/Talabu_Repair_Right.png",0,8,false);
	mPlay[RIGHT][DUCK] = new Animation("Player/Talabu_Ducking_Unarmed_Right.png",55,8,false);
	mPlay[RIGHT][JUMP] = new Animation("Player/Talabu_Jumping_Unarmed_Right.png",55,8,true);

	mPlay[LEFT][RUN] = new Animation("Player/Talabu_Running_Left_Unarmed_Spritesheet.png",55,8,false);
	mPlay[LEFT][IDLE] = new Animation("Player/Talabu_Idle_Left_Unarmed_Spritesheet.png",55,8,false);
	mPlay[LEFT][DUCK] = new Animation("Player/Talabu_Ducking_Unarmed_Left.png",55,8,false);
	mPlay[LEFT][JUMP] = new Animation("Player/Talabu_Jumping_Unarmed_Left.png",55,8,true);

	mDIRECTION = RIGHT;
	mTYPE = IDLE;

	mCurrentAnimation = mPlay[mDIRECTION][mTYPE];
}
void Player::gravity()
{
	mVelocity.y = 5;
	if(mVelocity.y > mMaxGravity)
		mVelocity.y = mMaxGravity;
	if(mVelocity.y < -mMaxGravity)
		mVelocity.y = -mMaxGravity;

	mHitBox.top += mVelocity.y;
}
void Player::magnetActions()
{
	if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		mMagnetVelocity = mDirection * mPullspeed;
		mHitBox.left += mMagnetVelocity.x;
		mHitBox.top += mMagnetVelocity.y;
	}
	if(sf::Mouse::isButtonPressed(sf::Mouse::Right))
	{
		mMagnetVelocity = mDirection * -mPullspeed;
		mHitBox.left += mMagnetVelocity.x;
		mHitBox.top += mMagnetVelocity.y;
	}

	
}
void Player::playerActions()
{
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		mVelocity.x = mMaxVelocity;
		mHitBox.left += mVelocity.x;
		mCurrentAnimation = mPlay[RIGHT][RUN];
	}
	else if(sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		mVelocity.x = -mMaxVelocity;
		mHitBox.left += mVelocity.x;
		mCurrentAnimation = mPlay[LEFT][RUN];
	}
	else
	{
		if(mVelocity.x > 0.1f)
		{
			mCurrentAnimation = mPlay[RIGHT][IDLE];
		}
		else if(mVelocity.x < -0.1f)
		{
			mCurrentAnimation = mPlay[LEFT][IDLE];
		}
		mVelocity.x = 0;
	}

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		mVelocity.y = -mMaxVelocity;
		mHitBox.top += mVelocity.y;
		mCurrentAnimation = mPlay[RIGHT][JUMP];
	}
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		mVelocity.y = mMaxVelocity;
		mHitBox.top += mVelocity.y;
		mCurrentAnimation = mPlay[LEFT][DUCK];
	}
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::O))
	{
		mCurrentAnimation = mPlay[RIGHT][REPAIR];
	}
}
void Player::update(entityVector &entities)
{	
	mLastPosition = mPosition; // G�r s� att han f�r sin gamla position igen f�r kolli

	std::cout << mLastPosition.x <<".x " << mLastPosition.y <<".y mLastPosition" << std::endl;
	////////
	//Magnet
	mMousePos = sf::Vector2f(gWindowManager->getWindow().convertCoords(sf::Mouse::getPosition(gWindowManager->getWindow()),gWindowManager->getView()));

	mDistance.x = mMousePos.x - mHitBox.left;
	mDistance.y = mMousePos.y - mHitBox.top;

	mLenghtofVector = sqrtf(powf(mDistance.x,2) + powf(mDistance.y,2));

	mDirection.x = mDistance.x / mLenghtofVector;
	mDirection.y = mDistance.y / mLenghtofVector;
	///////
	playerActions();
	magnetActions();
	gravity();

	mCurrentAnimation->setOrigin(sf::Vector2f(getCurrentAnimation()->getSprite().getGlobalBounds().width/7, getCurrentAnimation()->getSprite().getGlobalBounds().height/10));
	mCurrentAnimation->update();
	mCurrentAnimation->setPosition(sf::Vector2f(mHitBox.left, mHitBox.top));

	std::cout << mHitBox.left <<".x "<< mHitBox.top <<".y mPosition" <<  std::endl;

}
bool Player::isAlive()
{
	return mIsAlive;
}
Player::Category Player::getCategory()
{
	return PLAYER;
}
/*Test to get animation working i needed to add these two functions
	Erik �s�n													*/
const sf::Sprite& Player::getSprite() const
{
	return mCurrentAnimation->getSprite();
}
const sf::Vector2f Player::getPosition() const
{
	return mPosition;
}
const sf::Vector2f Player::getVelocity() const
{
	return mVelocity;
}
const sf::FloatRect Player::getHitbox() const
{
	return mHitBox;
}

void Player::setHitbox(sf::Vector2f pos)
{
	mHitBox.left = pos.x;
	mHitBox.top = pos.y;
}
void Player::draw()
{
	mCurrentAnimation->getSprite();
}
Animation* Player::getCurrentAnimation()
{
	return mCurrentAnimation; 
}
void Player::setPosition(const sf::Vector2f& position)
{
	mPosition = position;
}
void Player::setVelocity(const sf::Vector2f& velocity)
{
	mVelocity = velocity;
}
Player::~Player()
{
}