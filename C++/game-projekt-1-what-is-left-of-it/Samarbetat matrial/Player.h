#ifndef PLAYER_H
#define PLAYER_H
#include "Entity.h"

#include <map>

class Animation;
class SoundManager;
class Window_Manager;

class Player : public Entity
{
public:
	enum DIRECTION
	{
		LEFT,
		RIGHT
	};
	enum TYPE
	{
		RUN,
		IDLE,
		JUMP,
		REPAIR,
		DUCK		
	};
	Player();
	virtual ~Player();
	virtual void update(entityVector &entities);
	virtual const sf::Vector2f getPosition() const;
	virtual bool isAlive();
	virtual Category getCategory();
	virtual const sf::FloatRect getHitbox() const;
	virtual void draw();

	Animation *getCurrentAnimation();
	const sf::Sprite& getSprite() const;
	const sf::Vector2f getVelocity() const;
	void setPosition(const sf::Vector2f& position);
	void setVelocity(const sf::Vector2f& velocity);
	void setHitbox(const sf::Vector2f pos);

private:
	void magnetActions(); //Use the magnet this way
	void playerActions();
	void gravity();	

	//Enums for animation
	DIRECTION mDIRECTION;
	TYPE mTYPE;
	std::map <DIRECTION, std::map<TYPE, Animation*>> mPlay;
	Animation *mCurrentAnimation;
	//

	sf::Vector2f mVelocity;
	
	sf::Vector2f mPosition;
	sf::FloatRect mHitBox;
	sf::Vector2f mLastPosition;
	float mMaxVelocity;
	float mMaxGravity;
	bool mIsAlive;
	
	//Magnet varibels
	sf::Vector2f mDistance;
	sf::Vector2f mMousePos;
	sf::Vector2f mDirection;
	sf::Vector2f mMagnetVelocity;
	int mRange;
	float mPullspeed;
	float mLenghtofVector;
	


	sf::Event EventClick;

	//
};

#endif