#include "Animation.h"
#include "AssetManager.h"
#include "Window_Manager.h"

Animation::Animation(const std::string& filename, unsigned int timePerFrame, unsigned int numFrames, bool stop) : 
	mTimePerFrame(timePerFrame),
	mNumFrames(numFrames),
	mCurrentFrame(0),
	topLeft(0),
	topRight(0),
	mStop(stop)
{
	auto texture = gAssetManager->loadTexture("resources/Animation/" + filename);

	if(!texture == 0)
	{
		mSprite.setTexture(*texture);

		if(numFrames == 0)
			mSprite.setTextureRect(sf::IntRect(topLeft,topRight,mSprite.getTexture()->getSize().x,mSprite.getTexture()->getSize().y));
		else
			mSprite.setTextureRect(sf::IntRect(topLeft,topRight,mSprite.getTexture()->getSize().x/numFrames,mSprite.getTexture()->getSize().y));
	}
}
/*DETTA FUNKAR HAR TESTAT I ETT ANNAT PROJEKT*/ 
void Animation::update()
{
	if(mTimer.getElapsedTime().asMilliseconds() > mTimePerFrame)
	{
		mTimer.restart();

		++mCurrentFrame;

		if(mCurrentFrame >= mNumFrames && mStop == false)
		{
			mCurrentFrame = 0;
		}
		if(mCurrentFrame >= mNumFrames && mStop == true)
		{
			mCurrentFrame = 7;
		}

		sf::IntRect currentRect = mSprite.getTextureRect();
		currentRect.left = currentRect.width * mCurrentFrame;
		
		mSprite.setTextureRect(currentRect);
	}
}
void Animation::setScale(const sf::Vector2f &scale)
{
	mSprite.setScale(scale);
}
void Animation::setPosition(const sf::Vector2f &position)
{
	mSprite.setPosition(position);
}
const sf::Sprite& Animation::getSprite() const
{
	return mSprite;
}
void Animation::setOrigin(const sf::Vector2f &origin)
{
	mSprite.setOrigin(origin);
}

void Animation::draw() const
{
	gWindowManager->draw(mSprite);
}
Animation::~Animation()
{
}