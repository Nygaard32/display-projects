/*/////////////////////////////////////////////////
	Writen by: Erik �s�n
	2013-02-07
 /////////////////////////////////////////////////*/
#ifndef ANIMATION_H
#define ANIMATION_H
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <string>

class Animation
{
public:
	/*	Animation need three kind of input data to work The first is the search way for the file.
		Second how long the the frame shall be keept open. Third how many frames the file/picture has.*/
	Animation(const std::string& filename,unsigned int timePerFrame, unsigned int numFrames,bool stop);
	~Animation();
	 //A function thats updates witch frame that shall be played. In update there is also a reset, that makes the animation start from frame 1 again.
	void update();
		//Setst he animated sprite on the called object
	void setPosition(const sf::Vector2f &position);
		//Gets the animated sprite so it can be called by the objekt that uses it.
	const sf::Sprite& getSprite() const;
		//Sets the Sprites origin on the current caller.
	void setOrigin(const sf::Vector2f &origin);
		//Gets the animated sprite so it can be called by the objekt that uses it.
	void setScale(const sf::Vector2f &scale);
	void draw() const;
private:
	sf::Sprite mSprite;
	sf::Texture mTexture;
	sf::Uint32 mTimePerFrame;
	sf::Clock mTimer;
	unsigned int mNumFrames;
	unsigned int mCurrentFrame;
	unsigned int topLeft;
	unsigned int topRight;
	bool mStop;
};

#endif