#include "Window_Manager.h"

Window_Manager *gWindowManager;

Window_Manager::Window_Manager():
mWindow(sf::VideoMode(1280,720,32),"Off-topic"/*,sf::Style::Fullscreen*/)
{
	mWindow.setVerticalSyncEnabled(true);
}

void Window_Manager::init()
{
	gWindowManager = new Window_Manager();
}

void Window_Manager::deInit()
{
	delete gWindowManager;
}

void Window_Manager::draw(const sf::Drawable &drawable, const sf::Shader *shader)
{
	if(shader)
	{
		sf::View view = getView();
		mWindow.setView(sf::View(sf::FloatRect(0, 0, mWindow.getSize().x, mWindow.getSize().y)));
		sf::RenderStates rs;
		rs.shader = shader;
		mWindow.draw(drawable, rs);
		mWindow.setView(view);
	}
	else
	{
		mWindow.draw(drawable);
	}
}

bool Window_Manager::pollEvent(sf::Event &event)
{
	return mWindow.pollEvent(event);
}

void Window_Manager::closeWindow()
{
	mWindow.close();
}

void Window_Manager::clearWindow(sf::Color color)
{
	mWindow.clear(color);
}

void Window_Manager::setView(const sf::View &view)
{
	mWindow.setView(view);
}

const sf::View& Window_Manager::getView() const
{
	return mWindow.getView();
}

sf::RenderWindow &Window_Manager::getWindow()
{
	return mWindow;
}

void Window_Manager::updateDisplay()
{
	mWindow.display();
}

