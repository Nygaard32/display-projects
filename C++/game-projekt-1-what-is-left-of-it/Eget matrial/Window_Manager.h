#ifndef WINDOW_MANAGER_H
#define WINDOW_MANAGER_H

#include <SFML/Graphics/RenderWindow.hpp>


class Window_Manager
{
public:
	static void init();
	static void deInit();

	void draw(const sf::Drawable &drawable, const sf::Shader *shader =0);
	bool pollEvent(sf::Event &event);
	void closeWindow();
	void updateDisplay();
	void clearWindow(sf::Color color = sf::Color::Black);
	void setView(const sf::View &view);
	const sf::View& getView() const;
	sf::RenderWindow &getWindow();

private:
	Window_Manager();
	Window_Manager(const Window_Manager&);
	Window_Manager& operator=(const Window_Manager&);

	sf::RenderWindow mWindow;	//Pekare wanted
};

#endif

extern Window_Manager *gWindowManager;