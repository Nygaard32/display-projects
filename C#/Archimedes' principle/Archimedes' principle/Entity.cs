﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.Window;

namespace Archimedes__principle
{
    abstract class Entity
    {
        public abstract void update();
        public abstract void draw(RenderWindow render, Vector2f Size);
        protected abstract void calcVolume();
        protected abstract void calcDensity();
        protected abstract void calcArea();
    }
}
