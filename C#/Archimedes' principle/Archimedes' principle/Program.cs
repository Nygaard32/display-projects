﻿using System;
using SFML.Audio;
using SFML.Window;
using SFML.Graphics;

namespace Archimedes__principle
{
    class Program
    {
        static void Main(string[] args)
        {
            Display window = new Display();
            window.Init();
            window.update();
        }
    }
}