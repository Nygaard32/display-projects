﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Window;
using SFML.Graphics;

namespace Archimedes__principle
{
    class Display
    {
        private Vector2f windowSizes = new Vector2f(800, 600);
        private RenderWindow app;
        Archimedes_Princip archi;

        private void onClose(object sender, EventArgs e)
        {
            app = (RenderWindow)sender;
            app.Close();
        }
        private void createWindow()
        {
            app = new RenderWindow(new VideoMode((uint)windowSizes.X, (uint)windowSizes.Y), "Archimedes' princip");
            app.Closed += new EventHandler(onClose);
        }
        public void Init()
        {
            createWindow();
            app.SetFramerateLimit(120);
            archi = new Archimedes_Princip();
        }

        public void update()
        {
            while(app.IsOpen())
            {
                app.SetFramerateLimit(120);
                app.DispatchEvents();
                app.Clear();
                archi.update(app, windowSizes);
                app.Display();              
            }
        }
    }
}
