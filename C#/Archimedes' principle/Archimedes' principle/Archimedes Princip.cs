﻿using System;
using SFML.Window;
using SFML.Graphics;
namespace Archimedes__principle
{
    class Archimedes_Princip
    {
        private float _Force;
        private float _NewVol; //The calculated volume in getRightVol
        private float _BallCreationMass = 0.01f;
        private float _BallCreationRad = 0.5f;
        private bool _once = true;
        private bool _toggle = false;
        private float _RadChanger = 0.01f;
        private float _MassChanger = 0.01f;
        private float _LiquidValueChanger = 0.01f;
        private Ball _Ball;
        private Liquid _Liq;
        private float _Gravity = 9.82f;
        private bool change = true;

        public Archimedes_Princip()
        {
            reset();
        }
        public void update(RenderWindow render, Vector2f Sizes)
        {
            consoleText();

            if (Keyboard.IsKeyPressed(Keyboard.Key.R)) {
                reset();
                change = true;
            }

            if (Mouse.IsButtonPressed(Mouse.Button.Left))
            {
                onClick(render);
                change = true;
            }
            if (_Ball != null)
            {
                _Ball.update();
                _Ball.draw(render, Sizes);

                if (Keyboard.IsKeyPressed(Keyboard.Key.Q) && !_toggle) {
                    _Ball.changeMass(-_MassChanger);
                    change = true;
                }
                if (Keyboard.IsKeyPressed(Keyboard.Key.W) && !_toggle) {
                    _Ball.changeMass(_MassChanger); change = true;
                }
                if (Keyboard.IsKeyPressed(Keyboard.Key.Q) && _toggle) {
                    _Ball.changeRad(-_RadChanger); change = true; }
                if (Keyboard.IsKeyPressed(Keyboard.Key.W) && _toggle) {
                    _Ball.changeRad(_RadChanger); change = true; }
            }
            else
            {
                if (Keyboard.IsKeyPressed(Keyboard.Key.Q) && !_toggle) {
                    _BallCreationMass -= _MassChanger; change = true; }
                if (Keyboard.IsKeyPressed(Keyboard.Key.W) && !_toggle) { 
                    _BallCreationMass += _MassChanger; change = true; }
                if (Keyboard.IsKeyPressed(Keyboard.Key.Q) && _toggle) { 
                    _BallCreationRad -= _RadChanger;  change = true; }
                if (Keyboard.IsKeyPressed(Keyboard.Key.W) && _toggle) { 
                    _BallCreationRad += _RadChanger; change = true; }
        }

            if (_Liq != null)
            {
                _Liq.update();
                _Liq.draw(render, Sizes);

                if (Keyboard.IsKeyPressed(Keyboard.Key.A) && !_toggle) {
                    _Liq.changeHeight(-_LiquidValueChanger); change = true; }
                if (Keyboard.IsKeyPressed(Keyboard.Key.S) && !_toggle) {
                    _Liq.changeHeight(_LiquidValueChanger); change = true; }
                if (Keyboard.IsKeyPressed(Keyboard.Key.A) && _toggle) {
                    _Liq.changeLength(-_LiquidValueChanger); change = true; }
                if (Keyboard.IsKeyPressed(Keyboard.Key.S) && _toggle) {
                    _Liq.changeLength(_LiquidValueChanger); change = true; }
                if (Keyboard.IsKeyPressed(Keyboard.Key.Z) && !_toggle) {
                    _Liq.changeWidth(-_LiquidValueChanger); change = true; }
                if (Keyboard.IsKeyPressed(Keyboard.Key.X) && !_toggle) {
                    _Liq.changeWidth(_LiquidValueChanger); change = true; }
                if (Keyboard.IsKeyPressed(Keyboard.Key.Z) && _toggle) {
                    _Liq.changeMass(-_LiquidValueChanger); change = true; }
                if (Keyboard.IsKeyPressed(Keyboard.Key.X) && _toggle) {
                    _Liq.changeMass(_LiquidValueChanger); change = true; }
                if (Keyboard.IsKeyPressed(Keyboard.Key.C) && _Ball!=null) { 
                    _Liq.setDensity(_Ball.Density); change = true; }
            }

            if (Keyboard.IsKeyPressed(Keyboard.Key.LControl))
            {
                _toggle = false;
                change = true;
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.LShift))
            {
                _toggle = true;
                change = true;
            }
            if (_Liq != null && _Ball != null)
                inLiquidChech(Sizes.Y/2.0f);
        }
        private float getRightVol(float border)
        {
            if ((_Ball.Pos.Y + _Ball.Rad) > border)
            {
                //Ball is fully submerged
                return _Ball.Volume;
            }
            else
            {
                //Parts of the ball is submerged
                _NewVol = (2.0f / 3.0f) * (float) Math.PI * (float) Math.Pow((border- (_Ball.Pos.Y - _Ball.Rad)), 3);
                return _NewVol;
            }
        }
        private void calcBuoyancy(float border)
        {
            _Force = getRightVol(border) * _Liq.Density * _Gravity;
            _Ball.addForce(new Vector2f(0, -_Force));
        }
        private void onClick(RenderWindow render)
        {
            Vector2f mouseTemp = new Vector2f(render.InternalGetMousePosition().X, render.InternalGetMousePosition().Y);
            if(_once)
            {
                _Ball = new Ball(mouseTemp, new Vector2f(), _BallCreationMass, _BallCreationRad);
                _once = false;
            }
            else
            {
                _Ball.clearVelo();
                _Ball.Pos = mouseTemp;
            }
        }
        private void inLiquidChech(float border)
        {
            if ((_Ball.Pos.Y - _Ball.Rad) > border)
            {
                calcBuoyancy(border);
                _Ball.InLiquid = true;
            }
            else
            {
                _Ball.InLiquid = false;
                _NewVol = 0;
            }
        }
        private void reset()
        {
            _Force = 0;
            _NewVol = 0;
            _BallCreationMass = 0.01f;
            _BallCreationRad = 0.5f;
            _once = true;
            _Liq = new Liquid(0.1f, 0.1f, 0.2f);
            _Ball = null;
        }
        private void consoleText()
        {
            if (_Ball == null && change)
            {
                Console.Clear();
                Console.Write("Controls Ball:\nDec mass with \"Q\", inc with \"W\".\nWhen toggle = true dec rad with \"Q\", inc with \"W\".\n" +
                "\nControls Liquid:\nDec height with \"A\", inc with \"S\"\nWhen toggle = true dec length with \"A\", inc with \"S\"\n" +
                "Dec width with \"Z\", inc with \"X\"\nWhen toggle = true dec mass with \"Z\", inc with \"X\"\nSet liquid density to ball density with \"C\"\n" +
                "\nSet toggle to True with \"LShift\", set to false with \"LCtrl\".\n");
                Console.WriteLine("\nLiquid values.\n" + "Volume: " + _Liq.Volume.ToString() + " m ^ 3.\n" + "Density: " + _Liq.Density.ToString() + " kg / m ^ 3.\n" +
                "Mass: " + _Liq.Mass.ToString() + " kg.\n" + "Length: " + _Liq.Length.ToString() + " m.\n" + "Width: " + _Liq.Width.ToString() + " m.\n" +
                "Height: " + _Liq.Height.ToString() + " m.\n");
                Console.WriteLine("Ball creation values.\n" + "Mass: " + _BallCreationMass.ToString() + " kg.\n" + "Radius: " + _BallCreationRad.ToString() + " m.\n");
                Console.WriteLine("\nToggle: " + _toggle.ToString() + ".");
                
                change = false;
            }
            if (change && _Ball != null)
            {
                Console.Clear();
                Console.Write("Controls Ball:\nDec mass with \"Q\", inc with \"W\".\nWhen toggle = true dec rad with \"Q\", inc with \"W\".\n" +
                "\nControls Liquid:\nDec height with \"A\", inc with \"S\"\nWhen toggle = true dec length with \"A\", inc with \"S\"\n" +
                "Dec width with \"Z\", inc with \"X\"\nWhen toggle = true dec mass with \"Z\", inc with \"X\"\nSet liquid density to ball density with \"C\"\n" +
                "\nSet toggle to True with \"LShift\", set to false with \"LCtrl\".\n");

                Console.WriteLine("\nLiquid values.\n" + "Volume: " + _Liq.Volume.ToString() + " m ^ 3.\n" + "Density: " + _Liq.Density.ToString() + " kg / m ^ 3.\n" +
                "Mass: " + _Liq.Mass.ToString() + " kg.\n" + "Length: " + _Liq.Length.ToString() + " m.\n" + "Width: " + _Liq.Width.ToString() + " m.\n" +
                "Height: " + _Liq.Height.ToString() + " m.\n");

                Console.WriteLine("Ball values.\n" + "Volume: " + _Ball.Volume.ToString() + " m^3.\n" + "Density: " + _Ball.Density.ToString() + " kg/m^3.\n" +
                "Mass: " + _Ball.Mass.ToString() + " kg.\n" + "Radius: " + _Ball.Rad.ToString() + " m.");
                Console.WriteLine("\nToggle: " + _toggle.ToString() + ".");
                change = false;
            }
        }
    }
}
