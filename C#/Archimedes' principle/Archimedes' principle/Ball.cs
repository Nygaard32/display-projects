﻿using System;
using SFML.Window;
using SFML.Graphics;

namespace Archimedes__principle
{
    class Ball : Entity
    {
        public Vector2f Pos;
        public bool InLiquid = false;
        public Vector2f Velocity
        {
            get;
            private set;
        }
        public float Area
        {
            get;
            private set;
        }
        public float Density
        {
            get;
            private set;
        }
        public float Volume
        {
            get;
            private set;
        }
        public float Rad
        {
            get;
            private set;
        }
        public float Mass
        {
            get;
            private set;
        }
        public float InvMass
        {
            get;
            private set;
        }

        private Vector2f Force;
        private Vector2f _AccelerationDueToGravity;
        private float _BigFloat = 99999.9999f;
        private float _Gravity = 9.82f;
        private float _Damp = 0.99f;
        private Vector2f ZeroVector = new Vector2f();
        private CircleShape Circle;
        private Vertex[] line;
        private Vector2f _Size;

        public Ball(Vector2f pos, Vector2f velo, float mass, float rad)
        {
            Pos = pos;
            Velocity = velo;
            Mass = Math.Abs(mass);
            Rad = Math.Abs(rad);
            if (mass != 0)
                InvMass = 1.0f / mass;
            else
                InvMass = 1.0f / _BigFloat;
            if(Rad != 0)
            {
                calcArea();
                calcVolume();
            }
            Circle = new CircleShape(Rad);
            Circle.OutlineColor = Color.Red;
            Circle.Position = pos;                       
        }
        public override void update()
        {
            handleGround();
            addGravityForce(new Vector2f(0, Mass * _Gravity));
            move(1.0f/120.0f);

            updateBall();
   
            Force = ZeroVector;
            _AccelerationDueToGravity = ZeroVector;
        }
        public override void draw(RenderWindow render, Vector2f Size)
        {
            _Size = Size;
            line = new Vertex[2] { new Vertex(Pos), new Vertex(Pos + Velocity)};
            render.Draw(Circle);
            render.Draw(line,(PrimitiveType)2);
        }
        protected override void calcVolume()
        {
            Volume = (4.0f / 3.0f) * (float)Math.PI * (Rad * Rad * Rad);
            calcDensity();
        }
        protected override void calcDensity()
        {
            Density = Mass / Volume;
        }
        protected override void calcArea()
        {
            Area = 4.0f * (float)Math.PI * (Rad * Rad);
        }
        public void addForce(Vector2f force)
        {
            Force += force;
        }
        public void changeMass(float value)
        {
            Mass += value;
            if (Mass <= 0)
            {
                Mass = 0.01f;
            }
            calcDensity();
        }
        public void changeRad(float value)
        {
            Rad += value;
            if (Rad <= 0)
            {
                Rad = 0.1f;
            }
            calcArea();
            calcVolume();
        }
        public void clearVelo()
        {
            Velocity = new Vector2f();
        }
        private void handleGround()
        {
            if (Pos.Y > _Size.Y - Circle.Radius * 2)
            {
                Velocity = ZeroVector;
                Pos.Y = _Size.Y - Circle.Radius * 2;
            }
        }
        private void addGravityForce(Vector2f force)
        {
            _AccelerationDueToGravity += force;
        }
        private void move(float deltaTime)
        {
            Pos += Velocity * deltaTime;
            Velocity += (_AccelerationDueToGravity + Force) * InvMass * deltaTime;
            if(InLiquid)
            {
                Velocity *= _Damp;
            }
        }
        private void updateBall()
        {
            Circle.Position = Pos;
            Circle.Radius = Rad; //One pixel 1 is 1 meter for the ball. No relation to the liquid tank.
        }
    }
}
