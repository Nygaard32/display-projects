﻿using System;
using SFML.Window;
using SFML.Graphics;

namespace Archimedes__principle
{
    class Liquid : Entity
    {
        public float Length
        {
            get;
            private set;
        }
        public float Width
        {
            get;
            private set;
        }
        public float Height
        {
            get;
            private set;
        }
        public float Density
        {
            get;
            private set;
        }
        public float Area
        {
            get;
            private set;
        }
        public float Volume
        {
            get;
            private set;
        }
        public float Mass
        {
            get;
            private set;
        }

        private bool _oneTime = true;
        private Vertex[] _Line;

        public Liquid(float length, float width, float height)
        {
            Length = Math.Abs(length);
            Height = Math.Abs(height);
            Width = Math.Abs(width);

            if(Length != 0 && Height != 0 && Width != 0)
            {
                calcArea();
                calcVolume();
            }
        }
        public override void update(){}
        public override void draw(RenderWindow render, Vector2f Size)
        {
            _Line = new Vertex[] { new Vertex(new Vector2f(0, Size.Y/2)), new Vertex(new Vector2f(Size.X, Size.Y/2)) };
            
            render.Draw(_Line, (PrimitiveType)2);
            //render.Draw(new RectangleShape(new Vector2f(Size.X, 100f))); 
            //render.Draw(Header);
        }
        public void changeLength(float value)
        {
            Length += value;
            if( Length <= 0)
            {
                Length = 0.01f;
            }
            calcArea();
            calcVolume();
        }
        public void changeWidth(float value)
        {
            Width += value;
            if (Width <= 0)
            {
                Width = 0.01f;
            }
            calcArea();
            calcVolume();
        }
        public void changeHeight(float value)
        {
            Height += value;
            if (Height <= 0)
            {
                Height = 0.01f;
            }
            calcArea();
            calcVolume();
        }
        public void changeMass(float value)
        {
            Mass += value;
            if (Mass <= 0)
            {
                Mass = 0.01f;
            }
            calcDensity();
        }
        public void setDensity(float set)
        {
            Density = set;
        }
        protected override void calcArea()
        {
            Area = Length * Width;
        }
        protected override void calcDensity()
        {
            Density = Mass / Volume;
        }
        protected override void calcVolume()
        {
                Volume = Area * Height;
                if (_oneTime)
                {
                    Mass = Volume * (float)Math.Pow(10, 3);
                    _oneTime = false;
                }
                calcDensity();
        }
    }
}
