package com.example.b12erias.b12erias_assignment1;

import android.app.Activity;
import android.util.ArrayMap;
import android.util.Log;
import android.webkit.JavascriptInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Nygaard on 2015-06-17.
 */
public class AppJavaScriptProxy {
    private Activity activity =  null;
    private String[] data;

    private Storage store = null;
    private float trueElementalDamage = 0,trueRawDamage = 0,trueAffinityDamage = 0,
            trueRawSharpnessDamage = 0, trueEleSharpnessDamage = 0, trueAffiSharpnessDamage;
    private String selectedWeapon;
    //private WebSettings webSettings;
    private static final ArrayMap<String, Float> typeMultiplier;
    static {
        typeMultiplier = new ArrayMap<String, Float>();
        typeMultiplier.put("Great Sword",4.8f);
        typeMultiplier.put("Longs Sword", 3.3f);
        typeMultiplier.put("Sword and Shield",1.4f);
        typeMultiplier.put("Dual Blades",1.4f);
        typeMultiplier.put("Hammer",5.2f);
        typeMultiplier.put("Hunting Horn",5.2f);
        typeMultiplier.put("Lance",2.3f);
        typeMultiplier.put("Gunlance",2.3f);
        typeMultiplier.put("Switch Axe",5.4f);
        typeMultiplier.put("Charge Blade",3.6f);
        typeMultiplier.put("Insect Glaive",3.1f);
        typeMultiplier.put("Bow",1.2f);
        typeMultiplier.put("Light Bowgun",1.3f);
        typeMultiplier.put("Heavy Bowgun",1.5f);
    }
    private static final ArrayMap<String, Float> normSharpness;
    static {
        normSharpness = new ArrayMap<String, Float>();
        normSharpness.put("Red",0.50f);
        normSharpness.put("Orange",0.75f);
        normSharpness.put("Yellow",1.00f);
        normSharpness.put("Green",1.05f);
        normSharpness.put("Blue",1.20f);
        normSharpness.put("White",1.32f);
        normSharpness.put("Purple",1.44f);
    }
    private static final ArrayMap<String, Float> eleSharpness;
    static {
        eleSharpness = new ArrayMap<String, Float>();
        eleSharpness.put("Red",0.25f);
        eleSharpness.put("Orange",0.50f);
        eleSharpness.put("Yellow",0.75f);
        eleSharpness.put("Green",1.00f);
        eleSharpness.put("Blue",1.06f);
        eleSharpness.put("White",1.12f);
        eleSharpness.put("Purple",1.20f);
    }

    public  AppJavaScriptProxy(Activity activity) {
        this.activity = activity;
        store = Storage.getInstance();
    }

    @JavascriptInterface
    public String getData() {
        reciveValues();
        Log.d("SendDataJS1", "sendDataCalled");
        JSONArray json = null;
        Log.d("SendDataJS1", "sendDataCalled2");
        try {
            json = new JSONArray(data);
            Log.d("SendDataJS1", "sendDataCalled3");
        } catch (JSONException e) {
            Log.d("SendDataJS1", "sendDataCalled4");
            e.printStackTrace();
            Log.d("SendDataJS1", "sendDataCalled5");
        }
        Log.d("SendDataJS1", "sendDataCalled6");
        return json.toString();

    }

    public void reciveValues() {
        calcElementalDamage(store.getEle());
        calcRawandAffiDamage(store.getPower(), store.getAffi(), store.getName());
        calcSharpnessDamage(store.getSharp());
        selectedWeapon =  store.getName();
        setDataContainer();
    }

    private void calcElementalDamage(float elefloat) {
        //This one was short :D
        trueElementalDamage = elefloat/10;
        //Log.d("EleDamage1", "TrueEle: " + trueElementalDamage);
    }

    private void calcRawandAffiDamage(float powerfloat, float affinity, String type){
        //Time to fetch some values
        trueRawDamage = powerfloat / typeMultiplier.get(type);
        //Log.d("RawDamage1", "TrueRaw: " + trueRawDamage);
        trueAffinityDamage = powerfloat * (1.0f+0.25f*(affinity/100f));
        trueAffinityDamage /= typeMultiplier.get(type);
        //Log.d("AffiDamage1", "TrueAffi: " + trueAffinityDamage);
    }

    private void calcSharpnessDamage(String sharpness) {
        trueEleSharpnessDamage = trueElementalDamage * eleSharpness.get(sharpness);
        //Log.d("TrueEleSharpness1", "TrueEleSharp: " + trueEleSharpnessDamage);
        trueRawSharpnessDamage = trueRawDamage * normSharpness.get(sharpness);
        //Log.d("TrueRawSharpness1", "TrueRawSharp: " + trueRawSharpnessDamage);
        trueAffiSharpnessDamage = trueAffinityDamage * normSharpness.get(sharpness);
        //Log.d("TrueAffiSharpness1", "TrueAffiSharp: " + trueAffiSharpnessDamage);
    }

    private void setDataContainer() {
        data = new String[] {"Selected weapon: "+ selectedWeapon,"Elemental dmg: " + tryParse(trueElementalDamage),
                "Raw dmg: " + tryParse(trueRawDamage), "Affinity dmg: " + tryParse(trueAffinityDamage)
                ,"RawSharpness dmg: " + tryParse(trueRawSharpnessDamage),"EleSharpness dmg: " + tryParse(trueEleSharpnessDamage),
                "AffiSharpness dmg: " + tryParse(trueAffiSharpnessDamage)};

    }

    String tryParse (float data) {
        String parsed;
        try {
            parsed = Float.toString(data);
        } catch (Exception e) {
            return "";
        }
        return parsed;
    }
}
