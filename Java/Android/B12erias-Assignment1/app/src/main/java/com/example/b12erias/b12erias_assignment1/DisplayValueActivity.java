package com.example.b12erias.b12erias_assignment1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.ArrayMap;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class DisplayValueActivity extends Activity {

    private WebView mWebView;

    public void returnClick(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_value);
        mWebView = (WebView) findViewById(R.id.resultWebView);
        //Run JavaScript
        WebSettings wSettings = mWebView.getSettings();
        wSettings.setJavaScriptEnabled(true);
        //Add proxy service that handles javaScript calls
        mWebView.addJavascriptInterface(new AppJavaScriptProxy(this), "androidAppProxy");
        Log.d("proxy_attributes", mWebView.toString());
        //Load web content
        mWebView.loadUrl("file:///android_asset/www/result_display.html");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_display_value, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
