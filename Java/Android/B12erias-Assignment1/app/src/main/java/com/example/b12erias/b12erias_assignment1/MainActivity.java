package com.example.b12erias.b12erias_assignment1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;


public class MainActivity extends Activity {
    private Spinner weapSpinner;
    private Spinner sharpSpinner;
    private EditText powerText, affiText, eleText;
    private String paseString;

    private Storage store = null;

    public void sendData(View view) {
        Intent intent = new Intent(this, DisplayValueActivity.class);
        //Get spinners selectedItem, this looks horrible :( (Can't be bother currently to fix it)
        weapSpinner = (Spinner) findViewById(R.id.typeSpinner);
        sharpSpinner = (Spinner) findViewById(R.id.sharpnessSpinner);

        //Get text from TextEdit, this looks horrible :( (Can't be bother currently to fix it)
        powerText = (EditText) findViewById(R.id.powerInput);
        affiText = (EditText) findViewById(R.id.affinityInput);
        eleText = (EditText) findViewById(R.id.elementalInput);
        //Sending values to Storage.
        store.setValues(tryParse(powerText), tryParse(affiText), tryParse(eleText),
                sharpSpinner.getSelectedItem().toString(),weapSpinner.getSelectedItem().toString());

        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SetSpinnersContent();
        store = Storage.getInstance();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void SetSpinnersContent(){
        weapSpinner = (Spinner) findViewById(R.id.typeSpinner);
        sharpSpinner = (Spinner) findViewById(R.id.sharpnessSpinner);

        ArrayAdapter<CharSequence> weapAdapter = ArrayAdapter.createFromResource(this,R.array.WP_array,
                android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> sharpAdapter = ArrayAdapter.createFromResource(this, R.array.Sharp_array,
                android.R.layout.simple_spinner_item);

        weapAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sharpAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        weapSpinner.setAdapter(weapAdapter);
        sharpSpinner.setAdapter(sharpAdapter);
    }
    private float tryParse(EditText text){
        paseString = text.getText().toString();
        try {
            return Integer.parseInt(paseString);
        } catch (NumberFormatException e) {
            return 0;
        }
    }
}
