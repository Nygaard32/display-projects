package com.example.b12erias.b12erias_assignment1;

import android.util.Log;

/**
 * Created by Nygaard on 2015-06-17.
 */
public class Storage {
    private static Storage instance = null;
    private float mPower = 0, mAffi = 0, mEle = 0;
    private String mSharp = "", mName = "";

    protected Storage() {}
    public static Storage getInstance() {
        if(instance == null) {
            instance = new Storage();
        }
        return  instance;
    }

    public void setValues(float power, float affi, float ele, String sharp, String name){
        ZeroThem();
        mPower = power;
        mAffi = affi;
        mEle = ele;
        mSharp = sharp;
        mName = name;
    }
    private void ZeroThem() {
        mPower = 0;
        mAffi = 0;
        mEle = 0;
        mSharp = "";
        mName = "";
    }

    public float getPower(){
        //Log.d("getMpower","" + mPower);
        return  mPower;
    }
    public float getAffi(){
        //Log.d("getMele", "" + mAffi);
        return  mAffi;
    }
    public float getEle(){
        //Log.d("getMaffi", "" + mEle);
        return  mEle;
    }
    public String getSharp(){
        //Log.d("getMsharp", mSharp);
        return mSharp;
    }
    public String getName(){
        //Log.d("getMname", mName);
        return mName;
    }
}
