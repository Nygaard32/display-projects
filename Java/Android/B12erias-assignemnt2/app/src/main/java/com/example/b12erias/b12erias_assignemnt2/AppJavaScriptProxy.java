package com.example.b12erias.b12erias_assignemnt2;

/**
 * Created by b12erias on 2015-07-01.
 */

import android.app.Activity;
import android.util.ArrayMap;
import android.util.Log;
import android.webkit.JavascriptInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AppJavaScriptProxy {
    private Activity activity =  null;
    private String[] data;

    private Storage store = null;


    public  AppJavaScriptProxy(Activity activity) {
        this.activity = activity;
        store = Storage.getInstance();
    }

    @JavascriptInterface
    public String getData() {
        setDataContainer();
        Log.d("SendDataJS1", "sendDataCalled");
        JSONArray json = null;
        Log.d("SendDataJS1", "sendDataCalled2");
        try {
            json = new JSONArray(data);
            Log.d("SendDataJS1", "sendDataCalled3");
        } catch (JSONException e) {
            Log.d("SendDataJS1", "sendDataCalled4");
            e.printStackTrace();
            Log.d("SendDataJS1", "sendDataCalled5");
        }
        Log.d("SendDataJS1", "sendDataCalled6");
        return json.toString();

    }

    private void setDataContainer() {
        Log.d("SetData", "Data1");
        data = new String[] {"Selected weapon: "+ store.getmName(),"Elemental dmg: " + store.getEle().toString(),
                "Raw dmg: " + store.getRaw().toString(), "Affinity dmg: " + store.getAffi().toString()
                ,"Raw Sharpness dmg: " + store.getRawSharp().toString(),"Elemental Sharpness dmg: " + store.getEleSharp().toString(),
                "Affinity Sharpness dmg: " + store.getAffiSharp().toString()};
        Log.d("SetData", "Data2");
    }
}
