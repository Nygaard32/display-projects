package com.example.b12erias.b12erias_assignemnt2;

import android.util.ArrayMap;
import android.util.Log;

/**
 * Created by b12erias on 2015-07-01.
 */
public class Calculator {
    private static Calculator instance = null;
    //private float EleDMG,Raw,AffiDMG, RawSharp, EleSharp, AffiSharp;
    private Float EleDMG,Raw,AffiDMG;

    private static final ArrayMap<String, Float> typeMultiplier;
    static {
        typeMultiplier = new ArrayMap<String, Float>();
        typeMultiplier.put("Great Sword",4.8f);
        typeMultiplier.put("Longs Sword", 3.3f);
        typeMultiplier.put("Sword and Shield",1.4f);
        typeMultiplier.put("Dual Blades",1.4f);
        typeMultiplier.put("Hammer",5.2f);
        typeMultiplier.put("Hunting Horn",5.2f);
        typeMultiplier.put("Lance",2.3f);
        typeMultiplier.put("Gunlance",2.3f);
        typeMultiplier.put("Switch Axe",5.4f);
        typeMultiplier.put("Charge Blade",3.6f);
        typeMultiplier.put("Insect Glaive",3.1f);
        typeMultiplier.put("Bow",1.2f);
        typeMultiplier.put("Light Bowgun",1.3f);
        typeMultiplier.put("Heavy Bowgun",1.5f);
    }
    private static final ArrayMap<String, Float> normSharpness;
    static {
        normSharpness = new ArrayMap<String, Float>();
        normSharpness.put("None",1.00f);
        normSharpness.put("Red",0.50f);
        normSharpness.put("Orange",0.75f);
        normSharpness.put("Yellow",1.00f);
        normSharpness.put("Green",1.05f);
        normSharpness.put("Blue",1.20f);
        normSharpness.put("White",1.32f);
        normSharpness.put("Purple",1.44f);
    }
    private static final ArrayMap<String, Float> eleSharpness;
    static {
        eleSharpness = new ArrayMap<String, Float>();
        eleSharpness.put("None",1.00f);
        eleSharpness.put("Red",0.25f);
        eleSharpness.put("Orange",0.50f);
        eleSharpness.put("Yellow",0.75f);
        eleSharpness.put("Green",1.00f);
        eleSharpness.put("Blue",1.06f);
        eleSharpness.put("White",1.12f);
        eleSharpness.put("Purple",1.20f);
    }

    protected Calculator() {}
    public static Calculator getInstance() {
        //Log.d("CalcIns", "0 " + instance);
        if(instance == null) {
            instance = new Calculator();
            //Log.d("CalcIns", "1 " + instance);
        }
        //Log.d("CalcIns", "2 " + instance);
        return  instance;
    }

    public Float calcEle(Float ele) {
        //This one was short :D
        EleDMG = ele/10;
        return EleDMG;
    }

    public Float calcRaw(Float power, String type) {
        Raw = power / typeMultiplier.get(type);
        return Raw;
    }

    public Float calcAffi(Float power, Float affi, String type) {
        AffiDMG = power * (1.0f+0.25f*(affi/100f));
        AffiDMG /= typeMultiplier.get(type);
        return AffiDMG;
    }

    public Float calcEleSharp(String sharpness) {
        //EleSharp = EleDMG * eleSharpness.get(sharpness);
        return EleDMG * eleSharpness.get(sharpness);
    }

    public float calcRawSharp(String sharpness) {
        //RawSharp = Raw * normSharpness.get(sharpness);
        return Raw * normSharpness.get(sharpness);
    }

    public float calcAffiSharp(String sharpness) {
        //AffiSharp = AffiDMG * normSharpness.get(sharpness);
        return AffiDMG * normSharpness.get(sharpness);
    }
}
