package com.example.b12erias.b12erias_assignemnt2;

/**
 * Created by b12erias on 2015-07-01.
 */

import android.util.Log;

public class Storage {
    private static Storage instance = null;
    private Calculator calc = null;
    private Float mPower, mAffi, mEle;
    private String mSharp = "", mName = "";

    protected Storage() {}
    public static Storage getInstance() {
        if(instance == null) {
            instance = new Storage();
        }
        return  instance;
    }

    public void setValues(float power, float affi, float ele, String sharp, String name){
        createInstances();
        mPower = power;
        mAffi = affi;
        mEle = ele;
        mSharp = sharp;
        mName = name;
        //storeInFIle(mName);
    }

    public Float getRaw() {
        //Log.d("CalcDMG", "Raw: " + calc.calcRaw(mPower,mName));
        return calc.calcRaw(mPower,mName);
    }
    public Float getAffi() {
        //Log.d("CalcDMG", "Affi: " + calc.calcAffi(mPower, mAffi, mName));
        return calc.calcAffi(mPower, mAffi, mName);
    }
    public Float getEle() {
        //Log.d("CalcDMG", "Ele: " + calc.calcEle(mEle));
        return calc.calcEle(mEle);
    }
    public Float getEleSharp() {
        //Log.d("CalcDMG", "EleSharp: " + calc.calcEleSharp(mSharp));
        return calc.calcEleSharp(mSharp);
    }
    public Float getRawSharp() {
        //Log.d("CalcDMG", "RawSharp: " + calc.calcRawSharp(mSharp));
        return calc.calcRawSharp(mSharp);
    }
    public Float getAffiSharp() {
        //Log.d("CalcDMG", "AffiSharp: " + calc.calcAffiSharp(mSharp));
        return calc.calcAffiSharp(mSharp);
    }
    public String getmName() {
        //Log.d("CalcDMG", "Name: " + mName);
        return mName;
    }

    private void createInstances() {
        calc = Calculator.getInstance();
    }
}
