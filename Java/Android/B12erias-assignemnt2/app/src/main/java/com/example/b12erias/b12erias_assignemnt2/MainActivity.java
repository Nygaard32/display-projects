package com.example.b12erias.b12erias_assignemnt2;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;


public class MainActivity extends Activity {
    private Spinner weapSpinner;
    private Spinner sharpSpinner;
    private EditText powerText, affiText, eleText;
    private String paseString;
    private String[] splitMessage;
    private String FileName = "MHDC_file";
    private Random rnd = new Random();
    private int lastColor = Color.rgb(238,238,238);

    private Storage store = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);;
        //Set spinners and EditText fields
        SetSpinnersContent();
        powerText = (EditText) findViewById(R.id.powerInput);
        affiText = (EditText) findViewById(R.id.affinityInput);
        eleText = (EditText) findViewById(R.id.elementalInput);
        //Create preferences
        SharedPreferences settings = getSharedPreferences("PrefColors", Color.rgb(238, 238, 238));
        lastColor = settings.getInt("PrefColor", Color.rgb(238, 238, 238)); ;
        //Set window color
        this.getWindow().getDecorView().setBackgroundColor(lastColor);
        //Get instance of storage
        store = Storage.getInstance();

    }

    public void sendData(View view) {
        Intent intent = new Intent(this, DisplayValueActivity.class);
        //Store values in the internal memory
        storeData(Integer.toString(weapSpinner.getSelectedItemPosition())+"#"+Integer.toString(sharpSpinner.getSelectedItemPosition())
        +"#"+tryParse(powerText)+"#"+tryParse(eleText)+"#"+tryParse(affiText));
        //Sending values to Storage.
        store.setValues(tryParse(powerText), tryParse(affiText), tryParse(eleText),
                sharpSpinner.getSelectedItem().toString(), weapSpinner.getSelectedItem().toString());


        startActivity(intent);
    }

    public void sendAbout(View view) {
        Intent intent = new Intent(this, About.class);
        startActivity(intent);
    }

    public void savePref(View view) {
       //Log.d("COlor2", "1 " + lastColor);
       lastColor = rnd.nextInt();
       //Log.d("COlor2", "2 " + lastColor);
       SharedPreferences settings = getSharedPreferences("PrefColors", Color.rgb(238, 238, 238));
       SharedPreferences.Editor editor = settings.edit();
       editor.putInt("PrefColor", lastColor);
       editor.commit();
       //Log.d("COlor2", "3 " + settings.getInt("PrefColor", Color.rgb(238, 238, 238)));
       this.getWindow().getDecorView().setBackgroundColor(lastColor);
    }

    public void readFile(View view) {
        try {
            FileInputStream fin = openFileInput(FileName);
            int i;
            String temp = "";
            while ((i = fin.read()) != -1) {
                temp = temp + Character.toString((char) i);
            }
            splitMessage = temp.split("#",5);
            //Log.d("SPLITING", "START");
            weapSpinner.setSelection(Integer.parseInt(splitMessage[0]));
            //Log.d("SPLITING", "SPLIT0");
            sharpSpinner.setSelection(Integer.parseInt(splitMessage[1]));
            //Log.d("SPLITING", "SPLIT1");
            powerText.setText(splitMessage[2]);
            //Log.d("SPLITING", "SPLIT2");
            eleText.setText(splitMessage[3]);
            //Log.d("SPLITING", "SPLIT3");
            affiText.setText(splitMessage[4]);
            //Log.d("SPLITING", "SPLIT4");
            fin.close();
        } catch (FileNotFoundException e) {
            Log.d("LOG_readFile_Error", "FileNotFoundException: " + e.toString());
        } catch (IOException e) {
            Log.d("LOG_readFile_Error", "IOException: " + e.toString());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void SetSpinnersContent(){
        weapSpinner = (Spinner) findViewById(R.id.typeSpinner);
        sharpSpinner = (Spinner) findViewById(R.id.sharpnessSpinner);

        ArrayAdapter<CharSequence> weapAdapter = ArrayAdapter.createFromResource(this,R.array.WP_array,
                android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> sharpAdapter = ArrayAdapter.createFromResource(this, R.array.Sharp_array,
                android.R.layout.simple_spinner_item);

        weapAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sharpAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        weapSpinner.setAdapter(weapAdapter);
        sharpSpinner.setAdapter(sharpAdapter);
    }

    private int tryParse(EditText text){
        paseString = text.getText().toString();
        try {
            return Integer.parseInt(paseString);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private void storeData(String input) {
        //Log.d("storeData_LOG", input);
        try {
            FileOutputStream fos = openFileOutput(FileName, Context.MODE_PRIVATE);
            fos.write(input.getBytes());
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("LOG_loadWriteFile_Error", "FileNotFoundException: " + e.toString());
        } catch (IOException e) {
            Log.d("LOG_loadWriteFile_Error", "IOException: " + e.toString());
        }

    }
}