package com.b12erias.fav2;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.os.Environment.DIRECTORY_PICTURES;

// Todo: Create a about activity

public class Main extends Activity {

    /*Private*/
    private Spinner mWeapSpinner, mSharpSpinner, mMonsterSpinner;
    private ArrayAdapter<CharSequence> mWeapAdp, mSharpAdp, mMonAdp;
    private EditText mPowerText, mAffiText, mEleText;
    private String paseString;
    private Storage mStorage = null;
    private SharedPreferences mStoredPref;
    private SharedPreferences.Editor mPrefEdit;
    private String mParsed;
    private Intent mIntent;
    private ImageView mImg;
    private Bitmap mBitMapContainer[];
    private TextView mText;
    /*Public objects*/

    /*Protected methods*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View s = findViewById(R.id.relativeLayout);
        View root = s.getRootView();
        root.setBackgroundColor(getResources().getColor(android.R.color.background_light));

        loadImg();

        mStorage = Storage.getInstance();
        mStoredPref = getPreferences(MODE_PRIVATE);
        mPrefEdit = mStoredPref.edit();
        setSpinners();
        setEditText();
    }

    /*Public methods*/
    public void startCalc(View v) {
        mStorage.setValues(tryParse(mPowerText), tryParse(mAffiText), tryParse(mEleText),
                mSharpSpinner.getSelectedItem().toString(), mWeapSpinner.getSelectedItem().toString(),
                mMonsterSpinner.getSelectedItem().toString());
        setPrefs();
        //setText();
    }
    public void sendTo(View v) {
        mIntent = new Intent(this, About.class);
        startActivity(mIntent);
        finish();
    }
    public void loadPrefClick(View v) {
        loadPrefs();
    }
    @Override
    public void onConfigurationChanged(Configuration cfg) {
        super.onConfigurationChanged(cfg);
        if(cfg.orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.activity_main);
            setSpinners();
            setEditText();
            loadPrefs();
        } else if(cfg.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.activity_main_land);
            loadImg();
            setText();
            setImg();

        }
        Log.d("Config1", "End");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //public boolean isExternalStorageWritable() {
    //    String state = Environment.getExternalStorageState();
    //    if (Environment.MEDIA_MOUNTED.equals(state)) {
    //        return true;
    //    }
    //    return false;
    //}

    /*Private methods*/
    private void setText() {
        mText = (TextView) findViewById(R.id.textView);
        mText.setText(mStorage.getCompleteString());
    }
    private void loadImg() {
        Bitmap temp[] = {
            BitmapFactory.decodeResource(getResources(),R.drawable.seltas),
            BitmapFactory.decodeResource(getResources(),R.drawable.lagombi),
            BitmapFactory.decodeResource(getResources(),R.drawable.cephadrome),
            BitmapFactory.decodeResource(getResources(),R.drawable.none)};
        mBitMapContainer = temp;
    }
    private void setImg() {
        mImg = (ImageView) findViewById(R.id.MonsterView);
        mImg.setImageBitmap(mBitMapContainer[mMonsterSpinner.getSelectedItemPosition()]);
    }
    private void setPrefs() {
        //Log.d("Prefs", "SavePrefs1");
        mPrefEdit.putString("LastSwap", StringPrase());
        //Log.d("Prefs", "SavePrefs2");
        mPrefEdit.commit();
        //Log.d("Prefs", "SavePrefs2");
    }
    private void loadPrefs() {
        //Log.d("Prefs", "LoadPrefs1");
        Unparse(mStoredPref.getString("LastSwap", "No preferense found"));
        //Log.d("Prefs", "LoadPrefs2");

    }
    private void setEditText() {
        mPowerText = (EditText) findViewById(R.id.powerInput);
        mAffiText = (EditText) findViewById(R.id.affinityInput);
        mEleText = (EditText) findViewById(R.id.elementalInput);
    }
    private void setSpinners() {
        mWeapSpinner = (Spinner) findViewById(R.id.typeSpinner);
        mSharpSpinner = (Spinner) findViewById(R.id.sharpnessSpinner);
        mMonsterSpinner = (Spinner) findViewById(R.id.monsterSpinner);

        getContentToAdapters();

        mWeapSpinner.setAdapter(mWeapAdp);
        mSharpSpinner.setAdapter(mSharpAdp);
        mMonsterSpinner.setAdapter(mMonAdp);
    }
    private void getContentToAdapters() {
        mWeapAdp = ArrayAdapter.createFromResource(this,R.array.WP_array,
                android.R.layout.simple_spinner_item);
        mSharpAdp = ArrayAdapter.createFromResource(this, R.array.Sharp_array,
                android.R.layout.simple_spinner_item);
        mMonAdp = ArrayAdapter.createFromResource(this, R.array.Monster_array,
                android.R.layout.simple_spinner_item);

        mWeapAdp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSharpAdp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mMonAdp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }
    private int tryParse(EditText text){
        paseString = text.getText().toString();
        try {
            return Integer.parseInt(paseString);
        } catch (NumberFormatException e) {
            return 0;
        }
    }
    private String StringPrase() {
        mParsed = Integer.toString(mWeapSpinner.getSelectedItemPosition())+"#"+Integer.toString(mSharpSpinner.getSelectedItemPosition())
                +"#"+Integer.toString(mMonsterSpinner.getSelectedItemPosition())+"#"+tryParse(mPowerText) + "#" + tryParse(mEleText) + "#" + tryParse(mAffiText);
        return mParsed;
    }
    private void Unparse(String str) {
        if(str.contains("#")) {
            String splitMessage[] = str.split("#", 6);
            mWeapSpinner.setSelection(Integer.parseInt(splitMessage[0]));
            //Log.d("SPLITING", "SPLIT0: " + splitMessage[0]);
            mSharpSpinner.setSelection(Integer.parseInt(splitMessage[1]));
            //Log.d("SPLITING", "SPLIT1: " + splitMessage[1]);
            mMonsterSpinner.setSelection(Integer.parseInt(splitMessage[2]));
            //Log.d("SPLITING", "SPLIT2: " + splitMessage[2]);
            mPowerText.setText(splitMessage[3]);
            //Log.d("SPLITING", "SPLIT3: " + splitMessage[3]);
            mEleText.setText(splitMessage[4]);
            //Log.d("SPLITING", "SPLIT4: " + splitMessage[4]);
            mAffiText.setText(splitMessage[5]);
            //Log.d("SPLITING", "SPLIT5: " + splitMessage[5]);
        }
    }
}
