package com.b12erias.fav2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;


public class About extends Activity {
    private Intent mIntent;
    private WebView mWebView;

    public void back(View v) {
        mIntent = new Intent(this, Main.class);
        startActivity(mIntent);
        finish();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        View s = findViewById(R.id.AboutLayout);
        View root = s.getRootView();
        root.setBackgroundColor(getResources().getColor(android.R.color.background_light));

        mWebView = (WebView) findViewById(R.id.AboutWeb);
        mWebView.loadUrl("file:///android_asset/About.html");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_about, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
