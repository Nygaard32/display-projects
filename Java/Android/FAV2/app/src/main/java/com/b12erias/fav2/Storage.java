package com.b12erias.fav2;

import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

/**
 * Created by b12erias on 2015-08-11.
 * */
public class Storage {

    private static final ArrayMap<String, Float> mTypeMultiplier;
    static {
        mTypeMultiplier = new ArrayMap<String, Float>();
        mTypeMultiplier.put("Great Sword",4.8f);
        mTypeMultiplier.put("Longs Sword", 3.3f);
        mTypeMultiplier.put("Sword and Shield",1.4f);
        mTypeMultiplier.put("Dual Blades",1.4f);
        mTypeMultiplier.put("Hammer",5.2f);
        mTypeMultiplier.put("Hunting Horn",5.2f);
        mTypeMultiplier.put("Lance",2.3f);
        mTypeMultiplier.put("Gunlance",2.3f);
        mTypeMultiplier.put("Switch Axe",5.4f);
        mTypeMultiplier.put("Charge Blade",3.6f);
        mTypeMultiplier.put("Insect Glaive",3.1f);
    }
    private static final ArrayMap<String, Float> mNormSharpness;
    static {
        mNormSharpness = new ArrayMap<String, Float>();
        mNormSharpness.put("None",1.00f);
        mNormSharpness.put("Red",0.50f);
        mNormSharpness.put("Orange",0.75f);
        mNormSharpness.put("Yellow",1.00f);
        mNormSharpness.put("Green",1.05f);
        mNormSharpness.put("Blue",1.20f);
        mNormSharpness.put("White",1.32f);
        mNormSharpness.put("Purple",1.44f);
    }
    private static final ArrayMap<String, Float> mEleSharpness;
    static {
        mEleSharpness = new ArrayMap<String, Float>();
        mEleSharpness.put("None",1.00f);
        mEleSharpness.put("Red",0.25f);
        mEleSharpness.put("Orange",0.50f);
        mEleSharpness.put("Yellow",0.75f);
        mEleSharpness.put("Green",1.00f);
        mEleSharpness.put("Blue",1.06f);
        mEleSharpness.put("White",1.12f);
        mEleSharpness.put("Purple",1.20f);
    }
    private static final ArrayMap<String, String> mMonsterList; //This should have been a external file but can't be asked to do that right now 2015-08-13
    static {
        mMonsterList = new ArrayMap<String, String>();
        mMonsterList.put("Seltas","Head:50#Horn:35#Claw:42#Legs:45#Belly:65");
        mMonsterList.put("Lagombi","Head:45#Upper Body:44#Front Legs:30#Butt:63#Belly/Hind Legs:35");
        mMonsterList.put("Cephadrome","Head:35#Neck:65#Back/Wing:60#Belly:50#Wing Flaps:42#Legs:35#Tail:30");
    }
    private static final ArrayMap<String, String> mMonsterListHammer; //This should have been a external file but can't be asked to do that right now 2015-08-13
    static {
        mMonsterListHammer = new ArrayMap<String, String>();
        mMonsterListHammer.put("Seltas","Head:55#Horn:35#Claw:42#Legs:45#Belly:55");
        mMonsterListHammer.put("Lagombi","Head:63#Upper Body:44#Front Legs:30#Butt:50#Belly/Hind Legs:35");
        mMonsterListHammer.put("Cephadrome","Head:40#Neck:60#Back/Wing:50#Belly:55#Wing Flaps:35#Legs:40#Tail:30");
    }
    private static final ArrayMap<String, Float> mMotionValue;
    static {
        mMotionValue = new ArrayMap<String, Float>();
        mMotionValue.put("Great Sword",0.60f);
        mMotionValue.put("Longs Sword", 0.22f);
        mMotionValue.put("Sword and Shield",0.18f);
        mMotionValue.put("Dual Blades",0.11f);
        mMotionValue.put("Hammer",0.38f);
        mMotionValue.put("Hunting Horn",0.28f);
        mMotionValue.put("Lance",0.29f);
        mMotionValue.put("Gunlance",0.32f);
        mMotionValue.put("Switch Axe",0.33f);
        mMotionValue.put("Charge Blade",0.39f);
        mMotionValue.put("Insect Glaive",0.24f);
    }

    private static Storage instance = null;
    private Calculator calc = null;

    private Float mPower, mAffi, mEle;
    private Float mCalcPower, mCalcAffi, mCalcEle, mCalcSharpPow, mCalcSharpAffi, mCalcSharpEle, mMotionValueDMG, mMotionValueDMGAffi; //Calculated values

    private String mSharp = "", mName = "", mMonster = "", mCompleteString = "Damage\n";
    private Float mSplitValues[];
    private String mSplitNames[];

    protected Storage() {}

    public static Storage getInstance() {
        if(instance == null) {
            instance = new Storage();
        }
        return  instance;
    }
    public void setValues(float power, float affi, float ele, String sharp, String name, String monster){
        createInstances();
        mPower = power;
        mAffi = affi;
        mEle = ele;
        mSharp = sharp;
        mName = name;
        mMonster = monster;
        mCompleteString = "Damage\n";
        calc();
    }
    public String getCompleteString() {
        return mCompleteString;
    }

    private void createInstances() {
        calc = Calculator.getInstance();
    }
    private void calc(){
        mCalcPower = calc.calcRaw(mPower, mTypeMultiplier.get(mName));
        mCalcAffi = calc.calcAffi(mPower, mAffi, mTypeMultiplier.get(mName));
        mCalcEle = calc.calcEle(mEle);

        mCalcSharpPow = calc.calcRawSharp(mCalcPower, mNormSharpness.get(mSharp));
        mCalcSharpAffi = calc.calcAffiSharp(mCalcAffi, mNormSharpness.get(mSharp));
        mCalcSharpEle = calc.calcEleSharp(mCalcEle, mNormSharpness.get(mSharp));

        mMotionValueDMG = calc.calcMotionValue(mCalcSharpPow, mMotionValue.get(mName));
        mMotionValueDMGAffi = calc.calcMotionValueAffi(mCalcSharpAffi, mMotionValue.get(mName));
        Splitter();
        //Affinity willonly be used since it has an impact on all the dmg dealt with the weapon
        //Also it becomes esiers since i don't need to calculate the normal dmg(Can be implemented if needed)
        CompleteDMG();
    }
    private void CompleteDMG() {
        for( int i = 0; i < mSplitValues.length; i++) {
            mCompleteString += mSplitNames[i] + calc.calcDMGvsMonster(mMotionValueDMGAffi,mSplitValues[i]) + " |Ele: " +
                    calc.calcELEvsMonster(mCalcEle,mSplitValues[i])  + "\n";
        }
    }
    private void Splitter() {
        String temp;
        String tempArray[], nameArray[];
        Float floatArray[];
        if(!mName.startsWith("H")) {
            temp = mMonsterList.get(mMonster);
            tempArray = temp.split("#");
            floatArray = new Float[tempArray.length];
            nameArray = new String[tempArray.length];
            for(int i = 0; i < tempArray.length;i++) {
                floatArray[i] = Float.parseFloat(tempArray[i].substring(tempArray[i].length() - 2));
                nameArray[i] = tempArray[i].substring(0,tempArray[i].length()-2);
                //Log.d("Array", "" + nameArray[i]);
                //Log.d("Array", "" + floatArray[i]);
            }
            mSplitValues = floatArray;
            mSplitNames = nameArray;
        } else {
            temp = mMonsterListHammer.get(mMonster);
            tempArray = temp.split("#");
            floatArray = new Float[tempArray.length];
            nameArray = new String[tempArray.length];
            for(int i = 0; i < tempArray.length;i++) {
                floatArray[i] = Float.parseFloat(tempArray[i].substring(tempArray[i].length() - 2));
                nameArray[i] = tempArray[i].substring(0, tempArray[i].length() - 2);
                //Log.d("Array", "" + nameArray[i]);
                //Log.d("Array", "" + floatArray[i]);
            }
            mSplitValues = floatArray;
            mSplitNames = nameArray;
        }
    }
}
