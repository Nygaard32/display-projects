package com.b12erias.fav2;

/**
 * Created by b12erias on 2015-08-11.
 */
public class Calculator {
    private static Calculator instance = null;

    protected Calculator() {}
    public static Calculator getInstance() {
        //Log.d("CalcIns", "0 " + instance);
        if(instance == null) {
            instance = new Calculator();
            //Log.d("CalcIns", "1 " + instance);
        }
        //Log.d("CalcIns", "2 " + instance);
        return  instance;
    }

    public Float calcEle(Float ele) {
        //This one was short :D
        return ele/10;
    }

    public Float calcRaw(Float power, Float type) {
        return power / type;
    }

    public Float calcAffi(Float power, Float affi, Float type) {
        return ((power * (1.0f+0.25f*(affi/100f)))/type);
    }

    public Float calcEleSharp(Float eleDmg,Float sharpness) {
        return eleDmg * sharpness;
    }

    public Float calcRawSharp(Float rawPow,Float sharpness) {
        return rawPow * sharpness;
    }

    public Float calcAffiSharp(Float affiPow,Float sharpness) {
        //AffiSharp = AffiDMG * normSharpness.get(sharpness);
        return affiPow * sharpness;
    }

    public Float calcMotionValue (Float power, Float motionValueList) {
        return power * motionValueList;
    }

    public Float calcMotionValueAffi (Float Affipower, Float motionValueList) {
        return Affipower * motionValueList;
    }

    public Float calcDMGvsMonster(Float motionValueDMGAffi, Float monsterDefense) {
        return motionValueDMGAffi * (monsterDefense*0.01f);
    }

    public Float calcELEvsMonster(Float ele, Float monsterDefense) {
        return ele * (monsterDefense*0.01f);
    }

}
